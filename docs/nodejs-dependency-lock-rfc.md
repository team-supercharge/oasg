# RFC: Dependency Version Locking Mechanism for Node.js-based Targets

## Motivation

The Angular target in our project generation system has a known issue related to dependency versioning. The generated Angular project’s `package.json` defines multiple dependencies with flexible version constraints (e.g., using `^` or `~`), which allows for newer versions of dependencies to be pulled in automatically. This can lead to breaking changes, particularly when transitive dependencies are updated unexpectedly. A specific instance of this issue arose with Angular version 12, which introduced changes that broke builds of generated projects. The same could happen with all the Node.js-based targets within OASG.

As a temporary fix, the `targets/angular/generate.sh` script manually installs a specific, validated version of the problematic dependency. While this temporary solution has been effective, it is not a long-term fix and may not prevent future issues caused by other dependency changes or updates.

This RFC proposes a solution that introduces a mechanism to lock the `package-lock.json` to specific versions and reuse it when necessary.

## Proposed Solution

### High-Level Requirements

We considered the following requirements for the solution:
- It should prevent sudden breaking changes that could block any projects for a long time (e.g., a sudden change might delay a release for a day while hunting the exact cause of the issue and finding the right dependency versions).
- It should require minimal maintenance (e.g., it should not require the maintenance of dozens of `package-lock.json` files, one for every target client framework version).
- It should not require a new infrastructure element, such as a database, to store information.
- It should allow newer versions of dependencies to be installed where the flexible version constraints allow. This helps automatically follow vulnerability fixes up to a certain degree.

The proposed solution implements a dependency version locking mechanism that:
- Manages a known, working `package-lock.json` file for the current project.
- Prompts the developer to manually review and commit the `package-lock.json` to the project repository (where OASG is utilized) when it changes.
- Uses this saved `package-lock.json` when the target build fails due to dependency incompatibility.

### Solution Details

#### Workflow for `oasg generate`

1. **Building the target**:

   When running `oasg generate` for a Node.js-based target (e.g., Angular), the system will:
     - Run `npm install` and `npm build` to check if the build process completes successfully.
     - If `npm install` or `npm build` fails, it will attempt to resolve the issue using a saved `package-lock.json` (if available).
     - Force usage of the existing `package-lock.json` could be a command-line option to `oasg generate`.

2. **Successful Installation and Build**:

   If both `npm install` and `npm build` are successful, the system will check if there is a previously saved `package-lock.json` for the current target.
     - If a saved `package-lock.json` exists, it will compare it to the current `package-lock.json` in the project.
        - If the two `package-lock.json` files are identical, no further action is taken.
        - If the two `package-lock.json` files differ, the system will prompt the developer to manually update the saved lock file and commit the changes to version control. This ensures that smaller fixes, like security patches, are applied from time to time.
     - If no saved `package-lock.json` exists for the current target, the system will:
     	- Prompt the developer to copy the current build’s `package-lock.json` to the designated folder for saved lock files and commit it to version control.
     	- An `oasg` command would be provided to easily copy the current `package-lock.json` to the correct location.

3. **Failing Installation or Build**:

    If `npm install` or `npm build` fails, the system will:
     - Check for the presence of a saved `package-lock.json` for the target.
        - If a saved lock file exists, the system will copy it into the project directory and reattempt the `npm install` and `npm build` commands.
        - If no saved lock file exists, the process will fail.


#### Flow Diagram
```mermaid
flowchart TD
    A[Start] --> B[oasg calling openapi-generator]
    B --> C{npm install & build}

    C -->|Successful| D{Saved package-lock.json exists?}
    C -->|Failed| E{Target package-lock.json exists?}

    D -->|Yes| F{Packages identical?}
    D -->|No| G[Prompt: Copy package-lock.json]

    F -->|Yes| H[Do nothing]
    F -->|No| I[Prompt: Update package-lock.json]

    E -->|Yes| J[Copy package-lock.json]
    E -->|No| K[Prompt: Failure]

    J --> L{Retry npm install & build}

    L -->|Successful| H
    L -->|Failed| K

    G --> Z[End]
    H --> Z
    I --> Z
    K --> Z
```
#### Commands and User Prompts

- **oasg generate**: Runs the project generation process and checks dependencies as described.
- **oasg save-lock**: Copies the current `package-lock.json` to the designated folder for the current target, allowing developers to save the working lock file for future use.

#### Saved `package-lock.json` Location

The saved `package-lock.json` for each target will be stored in a central directory specific to each target or version. For example, a directory structure like the following could be used:

```
/locks
  /angular-12.0.0
    package-lock.json
  /angular-14.0.0
    package-lock.json
  /react-16.0.0
    package-lock.json
```

### Backward Compatibility

The new mechanism will not break existing workflows. The `oasg generate` process will continue to run without any issues if no saved `package-lock.json` is available. It will simply prompt the developer to save and commit the lock file after every successful build.
