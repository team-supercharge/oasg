const { oas } = require('@stoplight/spectral-rulesets');
const { casing, pattern } = require('@stoplight/spectral-functions');

const { schemaNames, pathCasingFactory, propertyCasingFactory } = require('./utils');

module.exports = {
  extends: oas,
  rules: {
    // Additional rules from `spectral:oas` ruleset
    'operation-singular-tag': true,
    'tag-description': true,

    // Object naming
    'oasg-object-names-pascal-case': {
      recommended: true,
      type: 'style',
      severity: 'error',
      description: 'Object names should be written in PascalCase',
      message: '`{{property}}` should be PascalCase (capitalized with no separator)',
      given: schemaNames,
      then: {
        function: casing,
        functionOptions: {
          type: 'pascal'
        }
      }
    },
    'oasg-object-names-api-model-suffix': {
      recommended: false,
      type: 'style',
      severity: 'warn',
      description: 'Object names should end with ApiModel suffix',
      message: '`{{property}}` should end with ApiModel suffix',
      given: schemaNames,
      then: {
        function: pattern,
        functionOptions: {
          match: '^.+ApiModel$'
        }
      }
    },

    // Operation ID naming
    'oasg-operation-ids-camel-case': {
      recommended: true,
      type: 'style',
      severity: 'error',
      description: 'Operation IDs should be written in camelCase',
      message: '`{{value}}` should be camelCase (start with lower and capitalize each word)',
      given: '$.paths.*.*.operationId',
      then: {
        function: casing,
        functionOptions: {
          type: 'camel'
        }
      }
    },

    // Path casing
    'oasg-path-casing-kebab': {
      recommended: true,
      ...pathCasingFactory('kebab'),
    },
    'oasg-path-casing-snake': {
      recommended: false,
      ...pathCasingFactory('snake'),
    },
    'oasg-path-casing-camel': {
      recommended: false,
      ...pathCasingFactory('camel'),
    },
    'oasg-path-casing-pascal': {
      recommended: false,
      ...pathCasingFactory('pascal'),
    },

      // Property casing
    'oasg-property-casing-kebab': {
      recommended: false,
      ...propertyCasingFactory('kebab'),
    },
    'oasg-property-casing-snake': {
      recommended: false,
      ...propertyCasingFactory('snake'),
    },
    'oasg-property-casing-camel': {
      recommended: true,
      ...propertyCasingFactory('camel'),
    },
    'oasg-property-casing-pascal': {
      recommended: false,
      ...propertyCasingFactory('pascal'),
    },
  },
};
