const { casing } = require('@stoplight/spectral-functions');
const { pathCasing } = require('../functions');

const schemaNames = [
  '$.components.schemas.*~',
  '$.components.requestBodies.*~',
  '$.components.responses.*~'
];

const casingOptions = {
  'kebab': {
    type: 'kebab',
    name: 'kebab-case',
    description: 'lower case and separated with hyphens'
  },
  'snake': {
    type: 'snake',
    name: 'snake_case',
    description: 'lower case and separated with underscores'
  },
  'camel': {
    type: 'camel',
    name: 'camelCase',
    description: 'start with lower and capitalize each word'
  },
  'pascal': {
    type: 'pascal',
    name: 'PascalCase',
    description: 'capitalized with no separator'
  }
};

function pathCasingFactory(type) {
  const options = casingOptions[type];

  return {
    type: 'style',
    severity: 'warn',
    given: '$.paths[*]~',
    description: `Path segments should be written in ${options.name}`,
    message: `\`{{property}}\` segments should be ${options.name} (${options.description})`,
    then: {
      function: pathCasing,
      functionOptions: {
        type: options.type
      }
    }
  };
};

function propertyCasingFactory(type) {
  const options = casingOptions[type];

  return {
    type: 'style',
    resolved: false,
    severity: 'warn',
    given: [
      '$.paths.*.*.requestBody.content.*.schema.properties[*]~',
      '$.paths.*.*.responses.*.content.*.schema.properties[*]~',
      '$.components.schemas.*.properties[*]~',
      '$.components.requestBodies.*.content.*.schema.properties[*]~',
      '$.components.responses.*.content.*.schema.properties[*]~',
    ],
    description: `Property names should be written in ${options.name}`,
    message: `\`{{property}}\` should be ${options.name} (${options.description})`,
    then: {
      function: casing,
      functionOptions: {
        type: options.type
      }
    }
  };
};

module.exports = {
  schemaNames,
  pathCasingFactory,
  propertyCasingFactory,
}
