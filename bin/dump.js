const fs = require('fs');
const yaml = require('js-yaml');

async function dump(document, fileName) {
  const content = yaml.dump(document, { lineWidth: -1 });
  fs.writeFileSync(fileName, content);

  return fileName;
}

exports.dump = dump;
