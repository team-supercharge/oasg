const crypto = require('crypto');
const fs = require('fs');
const SwaggerParser = require("@apidevtools/swagger-parser");

const { dump } = require(`${__dirname}/dump.js`);

async function applyOverrides(source, file) {
	if (!source.overrides) {
		return file;
	}

	const overrides = source.overrides;
	let outFile;

	if (!file.startsWith("./out/.tmp")) {
		if (!fs.existsSync('./out')) {
			fs.mkdirSync('./out');
		}

		if (!fs.existsSync('./out/.tmp')) {
			fs.mkdirSync('./out/.tmp');
		}

		outFile = `./out/.tmp/${source.id}-${crypto.randomBytes(4).readUInt32LE(0)}`;
	} else {
		outFile = file;
	}

	const document = await SwaggerParser.parse(file);

	if (overrides.info) {
		if (overrides.info.title) {
			document.info.title = overrides.info.title;
		}
		if (overrides.info.version) {
			document.info.version = overrides.info.version;
		}
		if (overrides.info.description) {
			document.info.description = overrides.info.description;
		}
		if (overrides.info.termsOfService) {
			document.info.termsOfService = overrides.info.termsOfService;
		}
	}

	if (overrides.externalDocs) {
		document.externalDocs = overrides.externalDocs;
	}

	if (overrides.contact) {
		document.info.contact = overrides.contact;
	}

	if (overrides.license) {
		document.info.license = overrides.license;
	}

	dump(document, outFile);

	return outFile;
}

exports.applyOverrides = applyOverrides;
