const fs = require('fs');
const PostmanParser = require('openapi-to-postmanv2');
const yaml = require('js-yaml');

const { dumpJSON } = require(`${__dirname}/dump-json.js`);

const defaultFileName = 'collection.json';

async function postmanTarget(target, source) {
  const outDir = `out/${target.id}`;
  let outFile = target.fileName || defaultFileName;
  outFile = `${outDir}/${outFile}`;

  if (fs.existsSync(outDir)) {
    fs.rmSync(outDir, { recursive: true })
  }
  fs.mkdirSync(outDir, { recursive: true });

  // Conversion options
  const defaultOptions = {
    requestNameSource: 'fallback',
    requestParametersResolution: 'schema',
    exampleParametersResolution: 'schema',
    parametersResolution: 'schema',
    collapseFolders: false,
    folderStrategy: 'tags',
    schemaFaker: false,
    optimizeConversion: false,
    includeAuthInfoInExample: false,
  };

   // Parse YAML to JavaScript object
  const spec = yaml.load(fs.readFileSync(source, 'utf8'));

  // Add version number in collection title
  spec.info.title = `${spec.info.title} (${spec.info.version})`;

  // Remove responses to avoid Example generation
  for (const pathKey in spec.paths) {
    const path = spec.paths[pathKey];
    for (const methodKey in path) {
      const operation = path[methodKey];

      operation.responses = [];
    }
  }

  // target-specific functions, e.g. write as JSON
  PostmanParser.convert({ type: 'string', data: spec },
    defaultOptions, (err, conversionResult) => {
      if (err) {
        console.error('Conversion error', err);
      }
      else if (!conversionResult.result) {
        console.log('Could not convert', conversionResult.reason);
      }
      else {
        // write file
        dumpJSON(conversionResult.output[0].data, outFile);
      }
    }
  );
}

exports.postmanTarget = postmanTarget;
