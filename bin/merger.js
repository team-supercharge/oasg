const crypto = require('crypto');
const fs = require('fs');
const path = require('path');
const SwaggerParser = require("@apidevtools/swagger-parser");
const { glob } = require('glob');

const { dump } = require(`${__dirname}/dump.js`);

async function merge(source) {
  if (!fs.existsSync('./out')) {
    fs.mkdirSync('./out');
  }

  const inputFiles = await getInputFilesWithGlob(source.inputs);

  if (inputFiles.length == 1) {
    return inputFiles[0];
  }

  if (!fs.existsSync('./out/.tmp')) {
    fs.mkdirSync('./out/.tmp');
  }
  const outFile = `./out/.tmp/${source.id}-${crypto.randomBytes(4).readUInt32LE(0)}.yaml`;

  const document = await mergeDocuments(inputFiles);

  dump(document, outFile);

  console.log(`merged input files =>\n  ${inputFiles.join('\n  ')}\ninto\n  ${outFile}\n`);

  return outFile;
}

/**
 * Get input files with glob while keeping the order of the input files.
 * @param {string[]} inputs The input for glob.
 * @returns {Promise<string[]>} The matched files.
 */
async function getInputFilesWithGlob(inputs) {
  /**
   * @type {string[]} inputFiles
   */
  const inputFiles = [];
  for (const input of inputs) {
    const files = await glob(input);
    inputFiles.push(...files.sort());
  }
  const uniqueInputFiles = inputFiles.filter((value, index, self) => self.indexOf(value) === index);
  return uniqueInputFiles;
}

async function mergeDocuments(documents) {
  let finalDoc = await SwaggerParser.parse(documents[0]);
  finalDoc = resolveExternalSpecifications(documents[0], finalDoc);

  for (var i = 1; i < documents.length; i++) {
    let nextDoc = await SwaggerParser.parse(documents[i]);
    nextDoc = resolveExternalSpecifications(documents[i], nextDoc);
    finalDoc = mergeDocument(finalDoc, nextDoc);
  }

  return finalDoc;
}

function resolveExternalSpecifications(currentFile, document) {
  const files = new Set([]);

  getAllReferencedFiles(document, files);

  if (files.size > 0) {
    files.forEach(refFile => {
      const newRefFile = getAbsolutePathForReferencedFile(currentFile, refFile);

      document = updateReferences(document, refFile, newRefFile);
    })
  } else {
    console.log('No external files');
  }

  return document;
}

function getAllReferencedFiles(document, files) {
  if (typeof document === 'object') {
    // iterating over the object using for..in
    for (var keys in document) {
      //checking if the current value is an object itself
      if (typeof document[keys] === 'object') {
        if (keys === "discriminator") {
          if ("mapping" in document[keys] && typeof document[keys].mapping === 'object') {
            for (var mappingKey in document[keys].mapping) {
              if (document[keys].mapping[mappingKey].startsWith(".")) {
                const externalFilePath = document[keys].mapping[mappingKey].split('#')[0];
                files.add(externalFilePath);
              }
            }
          }
          continue;
        }
        // if so then again calling the same function
        getAllReferencedFiles(document[keys], files)
      } else {
        // else checking for ref property, getting the file path if its an external local path
        if (keys === "$ref") {
          if (document[keys].startsWith(".")) {
            const externalFilePath = document[keys].split('#')[0];
            files.add(externalFilePath);
          }
        }
      }
    }
  }
  return document;
}

function getAbsolutePathForReferencedFile(currentFile, referencedFile) {
  const originalPath = process.cwd();

  process.chdir(path.dirname(`${originalPath}/${currentFile}`));

  const absolutePath = path.resolve(process.cwd(), referencedFile);

  // move back
  process.chdir(originalPath);

  return absolutePath;
}

function moveReferencedFiles(folderName, currentFile, files) {
  // save current process dir
  const originalPath = process.cwd();

  // create output temp directory
  const outputFolderPath = `${originalPath}/out/.tmp/${folderName}`;
  fs.mkdirSync(outputFolderPath);

  // move to original file directory, in order to find referenced files for copy command
  process.chdir(path.dirname(`${originalPath}/${currentFile}`));

  files.forEach(item => {
    const outName = path.basename(item);
    // copy referenced file to tmp output folder
    fs.copyFileSync(item, `${outputFolderPath}/${outName}`);
  });

  // move back
  process.chdir(originalPath);
}

function updateReferences(document, oldValue, newValue) {
  if (typeof document === 'object') {
    // iterating over the object using for..in
    for (var keys in document) {
      //checking if the current value is an object itself
      if (typeof document[keys] === 'object') {
        if (keys === "discriminator") {
          if ("mapping" in document[keys] && typeof document[keys].mapping === 'object') {
            for (var mappingKey in document[keys].mapping) {
              if (document[keys].mapping[mappingKey].includes(oldValue)) {
                document[keys].mapping[mappingKey] = document[keys].mapping[mappingKey].replace(oldValue, newValue);
              }
            }
          }
          continue;
        }
        // if so then again calling the same function
        updateReferences(document[keys], oldValue, newValue)
      } else {
        // else checking for ref property, if then replace old value with new one
        if (keys === "$ref") {
          if (document[keys].includes(oldValue)) {
            let keyValue = document[keys].replace(oldValue, newValue);
            document[keys] = keyValue;
          }
        }
      }
    }
  }
  return document;
}

function mergeDocument(leftDoc, rightDoc) {
  // not overriding: info, externalDocs (always from first file)
  // not merging: servers, security

  // paths
  const mergedPaths = { ...leftDoc.paths, ...rightDoc.paths };
  leftDoc.paths = mergedPaths;

  // webhooks (optional, only in 3.1.0)
  if (leftDoc.webhooks || rightDoc.webhooks) {
    const mergedWebhooks = { ...leftDoc.webhooks, ...rightDoc.webhooks };
    leftDoc.webhooks = mergedWebhooks;
  }

  // components
  const mergedSchemas = { ...leftDoc.components.schemas, ...rightDoc.components.schemas };
  const mergedResponses = { ...leftDoc.components.responses, ...rightDoc.components.responses };
  const mergedParameters = { ...leftDoc.components.parameters, ...rightDoc.components.parameters };
  const mergedExamples = { ...leftDoc.components.examples, ...rightDoc.components.examples };
  const mergedRequestBodies = { ...leftDoc.components.requestBodies, ...rightDoc.components.requestBodies };
  const mergedHeaders = { ...leftDoc.components.headers, ...rightDoc.components.headers };
  const mergedSecuritySchemes = { ...leftDoc.components.securitySchemes, ...rightDoc.components.securitySchemes };
  const mergedLinks = { ...leftDoc.components.links, ...rightDoc.components.links };
  const mergedCallbacks = { ...leftDoc.components.callbacks, ...rightDoc.components.callbacks };
  leftDoc.components.schemas = mergedSchemas;
  leftDoc.components.responses = mergedResponses;
  leftDoc.components.parameters = mergedParameters;
  leftDoc.components.examples = mergedExamples;
  leftDoc.components.requestBodies = mergedRequestBodies;
  leftDoc.components.headers = mergedHeaders;
  leftDoc.components.securitySchemes = mergedSecuritySchemes;
  leftDoc.components.links = mergedLinks;
  leftDoc.components.callbacks = mergedCallbacks;

  // tags
  const mergedTags = Array.from(new Set(leftDoc.tags.concat(rightDoc.tags)));
  leftDoc.tags = mergedTags;

  return leftDoc;
}

exports.merge = merge;
