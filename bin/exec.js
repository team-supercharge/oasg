const { exit } = require('process');
const { execSync } = require('child_process');

function exec(command, stdio) {
  try {
    execSync(command, { stdio: stdio || 'inherit' });
  }
  catch (e) {
    console.error(e.message);
    exit(1);
  }
}

function bash(command, stdio) {
  try {
    execSync(`bash -e -c "${command}"`, { stdio: stdio || 'inherit' });
  }
  catch (e) {
    console.error(e.message);
    exit(1);
  }
}

exports.exec = exec;
exports.bash = bash;
