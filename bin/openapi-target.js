const fs = require('fs');
const SwaggerParser = require("@apidevtools/swagger-parser");

const { dump } = require(`${__dirname}/dump.js`);

async function openApiTarget(target, source) {
  const outDir = `out/${target.id}`;
  let outFile = target.fileName || `openapi.yaml`;
  outFile = `${outDir}/${outFile}`;

  if (fs.existsSync(outDir)) {
    fs.rmSync(outDir, { recursive: true })
  }
  fs.mkdirSync(outDir, { recursive: true });

  // parse document
  let document = await SwaggerParser.parse(source);

  // target-specific functions, e.g. write as JSON

  // write file
  dump(document, outFile);
}

exports.openApiTarget = openApiTarget;
