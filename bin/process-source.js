const crypto = require('crypto');
const fs = require('fs');
const SwaggerParser = require("@apidevtools/swagger-parser");

const { dump } = require(`${__dirname}/dump.js`);
const { exec } = require(`${__dirname}/exec.js`);

function bundleSpec(source, target, remove) {
  exec(`npx redocly bundle ${source} --keep-url-references${remove ?  ' --remove-unused-components' : ''} --output ${target}`, 'pipe');
}

async function processSource(source, sourceFile, version) {
  const outDir = `./out/.tmp`;
  let outFile;

  if (!sourceFile.startsWith(outDir)) {
    if (!fs.existsSync(outDir)) {
      fs.mkdirSync(outDir, { recursive: true });
    }

    outFile = `${outDir}/${source.id}-${crypto.randomBytes(4).readUInt32LE(0)}.yaml`;
  }
  else {
    outFile = sourceFile;
  }

  // bundle spec to a single file by default
  if (source.bundle) {
    bundleSpec(sourceFile, outFile, false);
    console.log(`bundled specification`);
  }

  // parse document
  let document = await SwaggerParser.parse(source.bundle ? outFile : sourceFile);

  // set version of target document
  document.info.version = version;

  // sort schemas alphabeticaly
  if (source.sortSchemas && document.components && document.components.schemas) {
    document.components.schemas = Object.keys(document.components.schemas)
      .sort()
      .reduce((obj, key) => { obj[key] = document.components.schemas[key]; return obj; }, {});
  }

  // apply custom decorators
  const decorators = source.decorators || [];
  decorators.forEach(d => {
    const decoratorFile = `${d}.js`;
    const decoratorPath = `${process.cwd()}/${decoratorFile}`;

    if (!fs.existsSync(decoratorPath)) {
      console.error(`skipped decorator '${decoratorFile}' - file does not exist`);
      return;
    }

    const decorator = require(decoratorPath);

    if (!decorator.decorate) {
      console.error(`skipped decorator '${decoratorFile}' - must export a 'decorate(document)' function`);
      return;
    }

    document = decorator.decorate(document);

    console.log(`applied decorator '${decoratorFile}'`);
  });

  // clean up unused things
  if (source.cleanup) {
    const removedPaths = [];
    const removedWebhooks = [];
    const removedTags = [];
    const usedTags = [];

    // remove empty paths & gather tags
    const paths = document.paths || {};
    for (const pathKey in paths) {
      const path = paths[pathKey];

      if (Object.keys(path).length === 0) {
        removedPaths.push(pathKey);
        delete paths[pathKey];
      }

      for (const methodKey in path) {
        const maybeOperation = path[methodKey];

        if (!maybeOperation.tags) {
          continue;
        }

        maybeOperation.tags.forEach(tag => {
          if (!usedTags.includes(tag)) {
            usedTags.push(tag);
          }
        });
      }
    }

    // remove empty webhooks & gather tags
    const webhooks = document.webhooks || {};
    for (const webhookKey in webhooks) {
      const webhook = webhooks[webhookKey];

      if (Object.keys(webhook).length === 0) {
        removedWebhooks.push(webhookKey);
        delete webhooks[webhookKey];
      }

      for (const methodKey in webhook) {
        const maybeOperation = webhook[methodKey];

        if (!maybeOperation.tags) {
          continue;
        }

        maybeOperation.tags.forEach(tag => {
          if (!usedTags.includes(tag)) {
            usedTags.push(tag);
          }
        });
      }
    }

    if (removedPaths.length !== 0) {
      console.log(`cleaned up unused paths:\n  ${removedPaths.join('\n  ')}`);
    }

    if (removedWebhooks.length !== 0) {
      console.log(`cleaned up unused webhooks:\n  ${removedWebhooks.join('\n  ')}`);
    }

    // remouve unused tags
    const tags = [...document.tags];
    tags.forEach(tag => {
      if (!usedTags.includes(tag.name)) {
        removedTags.push(tag.name);
        document.tags.splice(document.tags.indexOf(tag), 1);
      }
    });

    if (removedTags.length !== 0) {
      console.log(`cleaned up unused tags:\n  ${removedTags.join('\n  ')}`);
    }
  }

  // write file
  dump(document, outFile);

  // re-bundle to remove unused components
  if (source.cleanup && source.bundle) {
    console.log(`re-bundling specification to remove unused components`);
    let currentSize;
    let bundledSize;
    do {
      currentSize = fs.statSync(outFile).size;
      bundleSpec(outFile, outFile, true);
      console.log('.');
      bundledSize = fs.statSync(outFile).size;
    }
    while (bundledSize !== currentSize)
  }

  return outFile;
}

exports.processSource = processSource;
