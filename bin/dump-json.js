const fs = require('fs');

async function dumpJSON(document, fileName) {
  const content = JSON.stringify(document, null, 2);
  fs.writeFileSync(fileName, content);
  return fileName;
}

exports.dumpJSON = dumpJSON;
