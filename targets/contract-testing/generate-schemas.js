const { generate } = require('openapi-typescript-validator-ext-ref');

const openapiPath = process.argv[2];
const outputPath = process.argv[3];

generate({
  addFormats: true,
  formatOptions: ['date', 'date-time', 'url', 'time', 'email', 'uuid', 'binary'],
  schemaFile: openapiPath,
  schemaType: 'yaml',
  directory: outputPath,
})
