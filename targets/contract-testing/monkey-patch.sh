#!/bin/bash

set -e
echo 'monkey-patching tools start...'

OUTPUT_PATH=$1;

### NODE http client
# monkey patch http error so it gives more detailed message for mocha reporter
sed -i.bak 's/super('\''HTTP request failed'\'');/super(`${statusCode} HTTP request failed: ${(response as any).req.path} Error: ${(response as any).body ? (response as any).body.message || JSON.stringify((response as any).body) : "Unknown error (no response.body)"}`);/g' $OUTPUT_PATH/api/apis.ts

### openapi schema

# 1. ajv config - show all validation error
sed -i.bak 's/const ajv = new Ajv({ strict: false });/const ajv = new Ajv({ strict: false, allErrors: true });/g' $OUTPUT_PATH/schema-validator/decoders.ts

# 2. fix missing format warning messages - https://github.com/Q42/openapi-typescript-validator/pull/9
# comment out original ajv.compile
sed -i.bak 's/ajv.compile(jsonSchema);/\/\/ajv.compile(jsonSchema); /g' $OUTPUT_PATH/schema-validator/decoders.ts
# add ajv.compile after format add|| body
sed -i.bak 's/\/\/ Decoders/ajv.compile(jsonSchema); /g' $OUTPUT_PATH/schema-validator/decoders.ts

# 3. dont trim JSON objects
sed -i.bak 's/.substring(0, 200);/.substring(0);/g' $OUTPUT_PATH/schema-validator/validate.ts

echo 'monkey-patching tools finished'
