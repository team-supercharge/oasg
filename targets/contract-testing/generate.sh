#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

node $(dirname "$0")/generate-schemas.js $openApiFile out/$targetId/schema-validator

# add exports
cat << EOF > out/$targetId/schema-validator/schema-validator.ts
export * from './decoders';
export * from './helpers';
export * from './meta';
export * from './models';
export * from './validate';
EOF

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -p "npmVersion=$version,npmName=$packageName,npmRepository=$repository" $generatorCustomArgs

echo "export * as schemaValidator from './schema-validator/schema-validator';" >> out/$targetId/api.ts

# apply monkey patches
$(dirname "$0")/monkey-patch.sh out/$targetId

# install additional dependencies
cd out/$targetId
npm install
# unnecessary dependency from typescript-node target
npm uninstall bluebird @types/bluebird
# schema validator dependencies
npm install typescript@4.2
npm install ajv@8.17.1 ajv-formats@2.1.1
# needed by typescript-node
npm install @types/node@8
npm run build -- --resolveJsonModule --esModuleInterop
cd ../..

