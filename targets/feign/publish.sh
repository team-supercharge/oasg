#/bin/bash

source $(dirname "$0")/../common.sh

settingsXml=${overrideSettingsXml:-$(dirname "$0")/settings.xml}
cp $settingsXml out/$targetId/settings.xml

cd out/$targetId

echo Deploying ${artifactId}-${version}.jar to ${mavenRepoUrl}

jarName=target/${artifactId}-${version}.jar

sourcesJarName=target/${artifactId}-${version}-sources.jar
attachSources=""
if [ -f ${sourcesJarName} ]; then
    attachSources="-Dsources=${sourcesJarName}"
fi

./mvnw deploy:deploy-file -e -s settings.xml \
                       -Durl=${mavenRepoUrl} \
                       -DrepositoryId=target \
                       -Dfile=${jarName} \
                       -Dpom=pom.xml \
                       -DgroupId=${groupId} \
                       -DartifactId=${artifactId} \
                       -Dversion=${version} \
                       ${attachSources}

cd ../..
