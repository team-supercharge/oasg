#!/bin/bash

# command line arguments:
#   1. version - semantic version string
#   2. binary - openapi-generator JAR file
#   3. configFile - path for the configuration file
#   4. targetId - ID of the target need to be generated
#   5. openApiFile - path for the Open API definition file
#   6. formatterBinary - path for formatter app binary
#   7. generatorId - openapi-code-generator identifier
#   8. preRelease - building/publishing a pre-release
#   9. templateDir - directory for merged templates

version=$1
binary=$2
configFile=$3
targetId=$4
openApiFile=$5
formatterBinary=$6
generatorId=$7
preRelease=$8
templateDir=$9

echo -e "\n=====\n  version:\t$version\n  binary:\t$binary\n  configFile:\t$configFile\n  targetId:\t$targetId\n generatorId:\t$generatorId\n openApiFile:\t$openApiFile\n  formatter:\t$formatterBinary\n  preRelease:\t$preRelease\n templateDir:\t$templateDir\n ---"

blacklist='.id, .type, .source, .generator, .generatorId, .templateDir'

params=$(jq -r ".targets[] | select(.id == \"$targetId\") | del($blacklist) | to_entries | map(\"\(.key)=\(.value | @sh);\") | .[]" $configFile)

echo "$params"
eval $params

echo -e "=====\n"
