#/bin/bash

source $(dirname "$0")/../common.sh

cd out
git clone $repository generator-source
rm -rf generator-source/$projectName
cp -rf $targetId/* generator-source

cd generator-source
if [ $(git status --porcelain | wc -l) -eq "0" ]; then
  echo "🟢 No changes"
else
  git add .
  git commit -m "chore(release): $version"

  if [ "$preRelease" == "false" ]; then git push; fi

  tagVersion="v$version"
  git tag -f $tagVersion
  git push origin -f --tags
fi

cd ..
rm -rf generator-source
cd ../..
