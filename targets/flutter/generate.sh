#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -c $(dirname "$0")/generator-config.json \
  -ppubLibrary=$packageName \
  -ppubName=$packageName \
  -ppubRepository=$repository \
  -ppubVersion=$version $generatorCustomArgs

cd out/$targetId
dart pub get
dart run build_runner build
cd ..
