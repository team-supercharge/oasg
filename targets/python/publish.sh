#/bin/bash

source $(dirname "$0")/../common.sh

cd out/$targetId
python3 -m twine upload --repository-url $repositoryUrl dist/*
cd ../..
