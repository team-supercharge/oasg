#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -c $(dirname "$0")/generator-config.json \
  -p "npmVersion=$version,npmName=$packageName,npmRepository=$repository" $generatorCustomArgs

cd out/$targetId

npm install
npm run build
cd ../..
