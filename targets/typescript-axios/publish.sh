#/bin/bash

source $(dirname "$0")/../common.sh

cd out/$targetId
npm publish \
  $(if [[ "$preRelease" == "true" ]]; then echo "--tag next"; fi)
cd ../..
