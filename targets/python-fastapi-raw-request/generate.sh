#/bin/bash

source $(dirname "$0")/../common.sh

# if the version contains a `-` character, then use PEP 440 beta version syntax
# example: 1.0.0-beta.1 -> 1.0.0b0+beta.1
if [[ $version == *"-"* ]]; then
  version=$(echo $version | sed 's/-/b0+/')
  echo "[NOTE] version updated to: ${version}"
fi

rm -rf out/$targetId
mkdir -p out/$targetId

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -c $(dirname "$0")/generator-config.json \
  -p "packageVersion=$version,packageName=$packageName" $generatorCustomArgs

cd out/$targetId

touch "src/$packageName/__init__.py"
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install flit==3.9.0
flit build --no-use-vcs

cd ../../
