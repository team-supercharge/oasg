#/bin/bash

source $(dirname "$0")/../common.sh

cd out/$targetId
source venv/bin/activate
pip install importlib_metadata==7.2.1 twine==4.0.2
python3 -m twine upload --repository-url $repositoryUrl dist/*
cd ../..
