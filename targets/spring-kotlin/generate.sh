#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -c $(dirname "$0")/generator-config.json \
  -p "artifactId=${artifactId},groupId=${groupId},basePackage=${basePackage},apiPackage=${basePackage}.api,modelPackage=${basePackage}.model,artifactVersion=${version}" $generatorCustomArgs

cd out/$targetId
cp $(dirname "$0")/mvnw .
cp -r $(dirname "$0")/.mvn .
./mvnw package
cd ../..
