#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId $generatorCustomArgs

cat out/$targetId/OpenAPIClient/*.yaml > out/$targetId/mock_server.yaml

cp $(dirname "$0")/Dockerfile out/$targetId/Dockerfile

if [[ "${generateWithDocker}" == "true" ]]; then
    cd out/$targetId
    docker build -t $repository:$version .
fi
