#/bin/bash

source $(dirname "$0")/../common.sh

cd out/$targetId

chmod a+x gradlew

./gradlew clean test publish -PrepoUrl=$repository

cd ../..
