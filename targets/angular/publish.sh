#/bin/bash

source $(dirname "$0")/../common.sh

cd out/$targetId/dist
npm publish \
  $(if [[ "$preRelease" == "true" ]]; then echo "--tag next"; fi)
