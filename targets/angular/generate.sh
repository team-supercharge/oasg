#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -c $(dirname "$0")/generator-config.json \
  -p "npmVersion=$version,npmName=$packageName,npmRepository=$repository" $generatorCustomArgs

cd out/$targetId
npm install
# This is a workaround for an issue with the angular generator. Root cause is the missing package-lock.json: https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator/src/main/resources/typescript-angular
# some of the transitive dependency has @types/node@": "*" in their package.json
# use "npm explain @types/node" in the generated folder (./out/$TARGET) to get more info
npm install --save-dev @types/node@20.17.0
npm run build
cd ../..
