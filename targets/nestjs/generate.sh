#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -c $(dirname "$0")/generator-config.json \
  -p "npmVersion=$version,npmName=$packageName,npmRepository=$repository" $generatorCustomArgs

cd out/$targetId

# remove unnecessary files
rm -rf variables.ts configuration.ts api.module.ts encoder.ts ng-package.json

# rewrite http method annotations
node rewrite-http-annotations.js
rm rewrite-http-annotations.js

# remove empty params objects from context objects
node remove-empty-param-objects-from-context.js
rm remove-empty-param-objects-from-context.js

# reorder operations (place ones with path params backwards)
node reorder-operations.js
rm reorder-operations.js

# unescape regex patterns
node unescape-patterns.js
rm unescape-patterns.js

# rewrite path parameters
node rewrite-path-parameters.js
rm rewrite-path-parameters.js

npm install
npm run build
cd ../..
