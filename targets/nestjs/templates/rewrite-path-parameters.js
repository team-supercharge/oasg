var fs = require('fs')

const apiFolder = './api';
const apiFileSuffix = '.api.ts';

// rewrite path parameters
const apiFiles = fs.readdirSync(apiFolder).filter(fn => fn.endsWith(apiFileSuffix));
apiFiles.forEach(apiFile => rewritePathParameters(apiFile));

function rewritePathParameters(fileName) {
    const file = `${apiFolder}/${fileName}`;
    const contents = fs.readFileSync(file, 'utf-8');

    var result = contents.replace(/\${this\.configuration\.encodeParam\({name:\s&quot;/g, ':');
    result = result.replace(/&quot;.*?}\)}/g, '');

    var options = { flag : 'w', encoding: 'utf8' };
    fs.writeFileSync(file, result, options);
}
