const fs = require('fs');

const apiFolder = './api';
const apiFileSuffix = '.api.ts';
const separator = '// ||||||||||\n';
const pathParamKey = 'this.configuration.encodeParam';

const apiFiles = fs.readdirSync(apiFolder).filter(fileName => fileName.endsWith(apiFileSuffix));
apiFiles.forEach(apiFile => reorderOperations(apiFile));

function reorderOperations(fileName) {
  const file = `${apiFolder}/${fileName}`;

  const contents = fs.readFileSync(file, 'utf-8');
  let operations = contents.split(separator);

  // first and last sections are not operations
  const header = operations.shift();
  const footer = operations.pop();

  // sort by containing path params or not
  operations = operations.sort((a, b) => {
    const aContains = a.includes(pathParamKey);
    const bContains = b.includes(pathParamKey);

    if (aContains === bContains) {
      return 0
    }
    else if (aContains && !bContains) {
      return 1;
    }
    else {
      return -1;
    }
  });

  fs.writeFileSync(file, [header, ...operations, footer].join(''));
}
