var fs = require('fs')

const apiFolder = './api';
const apiFileSuffix = '.api.ts';

// remove empty param objects from context objects
const apiFiles = fs.readdirSync(apiFolder).filter(fn => fn.endsWith(apiFileSuffix));
apiFiles.forEach(apiFile => removeEmptyParamObjects(apiFile));

function removeEmptyParamObjects(fileName) {
    const file = `${apiFolder}/${fileName}`;
    const contents = fs.readFileSync(file, 'utf-8');

    const result = contents.replace(/{ params: {  }, /g, '{ ');

    var options = { flag : 'w', encoding: 'utf8' };
    fs.writeFileSync(file, result, options);
}
