var fs = require('fs')

const apiFolder = './api';
const apiFileSuffix = '.api.ts';

// rewrite http annotations
const apiFiles = fs.readdirSync(apiFolder).filter(fn => fn.endsWith(apiFileSuffix));
apiFiles.forEach(apiFile => rewriteHttpAnnotations(apiFile));

function rewriteHttpAnnotations(fileName) {
    const file = `${apiFolder}/${fileName}`;
    const contents = fs.readFileSync(file, 'utf-8');

    var result = contents.replace(/  \/\/\/\/ @get/g, '  @Get');
    result = result.replace(/  \/\/\/\/ @post/g, '  @Post');
    result = result.replace(/  \/\/\/\/ @put/g, '  @Put');
    result = result.replace(/  \/\/\/\/ @delete/g, '  @Delete');
    result = result.replace(/  \/\/\/\/ @patch/g, '  @Patch');
    result = result.replace(/  \/\/\/\/ @options/g, '  @Options');
    result = result.replace(/  \/\/\/\/ @head/g, '  @Head');

    var options = { flag : 'w', encoding: 'utf8' };
    fs.writeFileSync(file, result, options);
}
