import { registerDecorator, buildMessage } from "class-validator";

export const IsValidISO8601Date = () => {
  return (object: Object, propertyName: string) => {
    registerDecorator({
      name: "isValidISO8601Date",
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: {},
      validator: {
        validate(value: string) {
          const [year, month, day] = value.split("-").map(Number);
          const date = new Date(year, month - 1, day);

          // Check if the date components are valid
          return (
            date.getFullYear() === year &&
            date.getMonth() === month - 1 &&
            date.getDate() === day
          );
        },
        defaultMessage: buildMessage(
          (eachPrefix) =>
            eachPrefix +
            "$property must be a valid ISO 8601 date string (YYYY-mm-DD format)"
        ),
      },
    });
  };
};
