import { BadRequestException, HttpExceptionOptions } from '@nestjs/common';

export class ApiContractValidationFailedException extends BadRequestException {
  constructor(objectOrError?: string | object | any, descriptionOrOptions?: string | HttpExceptionOptions) {
    super(objectOrError, descriptionOrOptions);
  }
}
