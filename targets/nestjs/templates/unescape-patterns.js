const fs = require('fs');

const modelFolder = './model';
const modelFileSuffix = '.ts';
const patternKey = '@Matches';

const modelFiles = fs.readdirSync(modelFolder).filter(fileName => fileName.endsWith(modelFileSuffix));
modelFiles.forEach(modelFile => unescapePatterns(modelFile));

function unescapePatterns(fileName) {
  const file = `${modelFolder}/${fileName}`;
  const contents = fs.readFileSync(file, 'utf-8');

  let lines = contents.split('\n');

  lines.forEach((line, index) => {
    if (!line.includes(patternKey)) {
      return;
    }

    // replace double backslashes with single backslashes
    lines[index] = line.replace(/\\\\/g, '\\');
  });

  fs.writeFileSync(file, lines.join('\n'));
}
