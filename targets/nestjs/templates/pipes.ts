import { ArgumentMetadata, PipeTransform, Optional } from '@nestjs/common';

import { ValidationPipe, ValidationPipeOptions, ValidationError } from '@nestjs/common';
import { ParseArrayPipe, ParseArrayOptions } from '@nestjs/common';

import { ApiContractValidationFailedException } from './exceptions';

export class OptionalParseIntPipe implements PipeTransform<string> {
  async transform(value: string, metadata: ArgumentMetadata): Promise<number | undefined> {
    if (value === undefined) {
      return undefined;
    }

    if (!this.isNumeric(value)) {
      throw new ApiContractValidationFailedException(
        'Validation failed (numeric string is expected)',
      );
    }
    return parseInt(value, 10);
  }

  private isNumeric(value: string): boolean {
    return (
      ['string', 'number'].includes(typeof value) &&
      /^-?\d+$/.test(value) &&
      isFinite(value as any)
    );
  }
}

export class OptionalParseFloatPipe implements PipeTransform<string> {
  async transform(value: string, metadata: ArgumentMetadata): Promise<number | undefined> {
    if (value === undefined) {
      return undefined;
    }

    if (!this.isNumeric(value)) {
      throw new ApiContractValidationFailedException(
        'Validation failed (numeric string is expected)',
      );
    }
    return parseFloat(value);
  }

  private isNumeric(value: string): boolean {
    return (
      ['string', 'number'].includes(typeof value) &&
      !isNaN(parseFloat(value)) &&
      isFinite(value as any)
    );
  }
}

export class OptionalParseBoolPipe implements PipeTransform<string | boolean> {
  async transform(value: string, metadata: ArgumentMetadata): Promise<boolean | undefined> {
    if (value === undefined) {
      return undefined;
    }

    if (this.isTrue(value)) {
      return true;
    }
    if (this.isFalse(value)) {
      return false;
    }
    throw new ApiContractValidationFailedException(
      'Validation failed (boolean string is expected)',
    );
  }

  private isTrue(value: string | boolean): boolean {
    return value === true || value === 'true';
  }

  private isFalse(value: string | boolean): boolean {
    return value === false || value === 'false';
  }
}

export class OptionalParseEnumPipe<T extends { [key: string]: any } = any> implements PipeTransform<T> {
  constructor(private readonly enumType: T) {
    if (!enumType) {
      throw new Error(
        `"OptionalParseEnumPipe" requires "enumType" argument specified (to validate input values).`,
      );
    }
  }

  async transform(value: T, metadata: ArgumentMetadata): Promise<T | undefined> {
    if (value === undefined) {
      return undefined;
    }

    if (!this.isEnum(value)) {
      throw new ApiContractValidationFailedException(
        'Validation failed (enum string is expected)',
      );
    }
    return value;
  }

  private isEnum(value: T): boolean {
    const enumValues = Object.keys(this.enumType).map(
      item => this.enumType[item],
    );
    return enumValues.includes(value);
  }
}

export class RequiredPipe implements PipeTransform<any, any> {
  async transform(value: unknown, metadata: ArgumentMetadata): Promise<any> {
    if (
      value === undefined ||
      value === null ||
      (typeof value === 'string' && value === '') ||
      (typeof value === 'object' && Object.keys(value).length === 0)
    ) {
      throw new ApiContractValidationFailedException(
        `Parameter ${metadata.data} in ${metadata.type} is required`,
      );
    }

    return value;
  }
}

export class ApiValidationPipe extends ValidationPipe {
  constructor(@Optional() options?: ValidationPipeOptions) {
    super(options);
  }

  public createExceptionFactory() {
    return (validationErrors: ValidationError[] = []) => {
      if (this.isDetailedOutputDisabled) {
        return new ApiContractValidationFailedException();
      }
      const message = this.flattenValidationErrors(validationErrors).join(' | ');
      return new ApiContractValidationFailedException(message);
    };
  }
}

export class ApiParseArrayPipe extends ParseArrayPipe {
  constructor(@Optional() protected readonly options: ParseArrayOptions = {}) {
    options.exceptionFactory = (error) => new ApiContractValidationFailedException(error);
    super(options);
  }
}
