#!/bin/bash

source $(dirname "$0")/../common.sh

cd out/$targetId

# pack
dotnet restore
dotnet build -c Release
dotnet pack -c Release -p:Version=$version -p:IsPackable=true

isWindows=false
if [[ "$OSTYPE" == "cygwin"* ]] || [[ "$OSTYPE" == "msys"* ]]; then
    isWindows=true
fi
storePasswordOption=$([[ $isWindows == true ]] && echo "" || echo "--store-password-in-clear-text")

# publish
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?><configuration></configuration>" > nuget.config
dotnet nuget add source "${sourceUrl}" -n Feed -u User -p "${apiKey}" --configfile nuget.config $storePasswordOption
dotnet nuget push "src/**/bin/Release/*.nupkg" -s ${sourceUrl} -k AZ

cd ../..
