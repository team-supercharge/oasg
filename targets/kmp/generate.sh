#/bin/bash

source $(dirname "$0")/../common.sh

rm -rf out/$targetId
mkdir -p out/$targetId

if [ -z "$formatterCustomArgs" ]
then
  formatterCustomArgs="--disabled_rules=no-wildcard-imports,max-line-length,enum-entry-name-case,filename"
fi

java -jar $binary generate \
  -g $generatorId \
  -i $openApiFile \
  -t $templateDir \
  -o out/$targetId \
  -c $(dirname "$0")/generator-config.json \
  -p "artifactId=${artifactId},groupId=${groupId},packageName=${packageName},artifactVersion=${version}" $generatorCustomArgs

$formatterBinary -F "out/$targetId/src/**/*.kt" $formatterCustomArgs
