#/bin/bash

source $(dirname "$0")/../common.sh

cd out/$targetId

chmod a+x gradlew

./gradlew clean publish -PrepoUrl=$repository

cd ../..
