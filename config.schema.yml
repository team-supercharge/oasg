type: object
properties:
  sources:
    type: array
    items:
      oneOf:
        - $ref: '#/sources/Simple'
        - $ref: '#/sources/Merged'
  targets:
    type: array
    items:
      oneOf:
        - $ref: '#/targets/Angular'
        - $ref: '#/targets/React'
        - $ref: '#/targets/Feign'
        - $ref: '#/targets/FeignKotlin'
        - $ref: '#/targets/Spring'
        - $ref: '#/targets/SpringKotlin'
        - $ref: '#/targets/Stubby'
        - $ref: '#/targets/Android'
        - $ref: '#/targets/ios'
        - $ref: '#/targets/Python'
        - $ref: '#/targets/PythonLegacy'
        - $ref: '#/targets/PythonFastApi'
        - $ref: '#/targets/PythonFastApiRawRequest'
        - $ref: '#/targets/ContractTesting'
        - $ref: '#/targets/NestJS'
        - $ref: '#/targets/OpenAPI'
        - $ref: '#/targets/Flutter'
        - $ref: '#/targets/Kmp'
        - $ref: '#/targets/Dotnet'
        - $ref: '#/targets/Postman'
        - $ref: '#/targets/TypeScriptAxios'
        - $ref: '#/targets/TypeScriptFetch'
required:
  - targets
additionalProperties: false

sources:
  # base properties
  Base:
    type: object
    properties:
      id:
        type: string
        pattern: '^[a-zA-Z0-9]+([-_][a-zA-Z0-9]+)*$' # only kebab-case or snake_case IDs
      type:
        type: string
      bundle:
        type: boolean
      sortSchemas:
        type: boolean
      decorators:
        type: array
        items:
          type: string
      cleanup:
        type: boolean
      overrides:
        $ref: '#/definitions/SourceOverrides'
    required:
      - id
      - type
    additionalProperties: false

  # source types
  Simple:
    allOf:
      - $ref: '#/sources/Base'
      - properties:
          type:
            pattern: "^simple$"
          input:
            type: string
        required:
          - input

  Merged:
    allOf:
      - $ref: '#/sources/Base'
      - properties:
          type:
            pattern: "^merged$"
          inputs:
            type: array
            items:
              type: string
        required:
          - inputs

targets:
  # base properties
  Base:
    type: object
    properties:
      id:
        type: string
        pattern: '^[a-zA-Z0-9]+([-_][a-zA-Z0-9]+)*$' # only kebab-case or snake_case IDs
      type:
        type: string
      source:
        type: string
      generator:
        type: string
        pattern: '^(\d+\.\d+\.\d+(-.+)?)|(^http(s)?:\/\/.+$)$' # matches: 0.0.0(-pre) and http(s)://XXX
      generatorId:
        type: string
      generatorCustomArgs:
        type: string
      templateDir:
        type: string
    required:
      - id
      - type
    additionalProperties: false

  # target types
  Angular:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^angular$"
          packageName:
            type: string
          repository:
            type: string
        required:
          - packageName
          - repository

  React:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^react$"
          packageName:
            type: string
          repository:
            type: string
        required:
          - packageName
          - repository

  Feign:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^feign$"
          artifactId:
            type: string
          groupId:
            type: string
          basePackage:
            type: string
          mavenRepoUrl:
            type: string
          overrideSettingsXml:
            type: string
        required:
          - artifactId
          - groupId
          - basePackage
          - mavenRepoUrl

  FeignKotlin:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^feign-kotlin$"
          artifactId:
            type: string
          groupId:
            type: string
          basePackage:
            type: string
          mavenRepoUrl:
            type: string
          overrideSettingsXml:
            type: string
        required:
          - artifactId
          - groupId
          - basePackage
          - mavenRepoUrl

  Spring:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^spring$"
          artifactId:
            type: string
          groupId:
            type: string
          basePackage:
            type: string
          mavenRepoUrl:
            type: string
          overrideSettingsXml:
            type: string
        required:
          - artifactId
          - groupId
          - basePackage
          - mavenRepoUrl

  SpringKotlin:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^spring-kotlin$"
          artifactId:
            type: string
          groupId:
            type: string
          basePackage:
            type: string
          mavenRepoUrl:
            type: string
          overrideSettingsXml:
            type: string
        required:
          - artifactId
          - groupId
          - basePackage
          - mavenRepoUrl

  Stubby:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^stubby$"
          repository:
            type: string
          generateWithDocker:
            type: string
            pattern: 'true|false'
          required:
            - repository
            - generateWithDocker

  Android:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^android$"
          packageName:
            type: string
          groupId:
            type: string
          artifactId:
            type: string
          formatter:
            type: string
            pattern: '^(\d+\.\d+\.\d+(-.+)?)|(^http(s)?:\/\/.+$)$' # matches: 0.0.0(-pre) and http(s)://XXX
          formatterCustomArgs:
            type: string
          repository:
            type: string
        required:
        - packageName
        - groupId
        - artifactId
        - repository

  ios:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^ios$"
          projectName:
            type: string
          repository:
            type: string
          interfaceType:
            type: string
            pattern: 'Combine|RxSwift|AsyncAwait|PromiseKit'
        required:
        - projectName
        - repository
        - interfaceType

  Python:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^python$"
          packageName:
            type: string
          repositoryUrl:
            type: string
        required:
        - packageName
        - repositoryUrl

  PythonLegacy:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^python-legacy$"
          packageName:
            type: string
          repositoryUrl:
            type: string
        required:
        - packageName
        - repositoryUrl

  PythonFastApi:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^python-fastapi$"
          packageName:
            type: string
          repositoryUrl:
            type: string
        required:
        - packageName
        - repositoryUrl

  PythonFastApiRawRequest:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^python-fastapi-raw-request$"
          packageName:
            type: string
          repositoryUrl:
            type: string
        required:
        - packageName
        - repositoryUrl

  ContractTesting:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^contract-testing$"
          packageName:
            type: string
          repository:
            type: string
        required:
          - packageName
          - repository

  NestJS:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^nestjs$"
          packageName:
            type: string
          repository:
            type: string
        required:
          - packageName
          - repository

  OpenAPI:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^openapi$"
          fileName:
            type: string

  Flutter:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^flutter$"
          packageName:
            type: string
          repository:
            type: string
        required:
        - packageName
        - repository

  Dotnet:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: '^dotnet$'
          sourceUrl:
            type: string
          apiKey:
            type: string
          packageName:
            type: string
        required:
          - sourceUrl
          - apiKey
          - packageName

  Kmp:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^kmp$"
          packageName:
            type: string
          groupId:
            type: string
          artifactId:
            type: string
          formatter:
            type: string
            pattern: '^(\d+\.\d+\.\d+(-.+)?)|(^http(s)?:\/\/.+$)$' # matches: 0.0.0(-pre) and http(s)://XXX
          formatterCustomArgs:
            type: string
          repository:
            type: string
        required:
        - packageName
        - groupId
        - artifactId
        - repository

  Postman:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^postman$"
          fileName:
            type: string

  TypeScriptAxios:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^typescript-axios$"
          packageName:
            type: string
          repository:
            type: string
        required:
          - packageName
          - repository

  TypeScriptFetch:
    allOf:
      - $ref: '#/targets/Base'
      - properties:
          type:
            pattern: "^typescript-fetch$"
          packageName:
            type: string
          repository:
            type: string
        required:
          - packageName
          - repository

definitions:
  # default
  SourceOverrides:
    type: object
    properties:
      info:
        $ref: '#/definitions/SourceOverrideInfo'
      externalDocs:
        $ref: '#/definitions/SourceOverrideExternalDocumentation'
      contact:
        $ref: '#/definitions/SourceOverrideContact'
      license:
        $ref: '#/definitions/SourceOverrideLicense'
    additionalProperties: false

  # info
  SourceOverrideInfo:
    type: object
    properties:
      title:
        type: string
      description:
        type: string
      termsOfService:
        type: string
      version:
        type: string
    required:
      - title
      - version
    additionalProperties: false

  # contact
  SourceOverrideContact:
    type: object
    properties:
      name:
        type: string
      url:
        type: string
      email:
        type: string
    additionalProperties: false

  # license
  SourceOverrideLicense:
    type: object
    properties:
      name:
        type: string
      url:
        type: string
    required:
      - name
    additionalProperties: false

  # externalDocumentation
  SourceOverrideExternalDocumentation:
    type: object
    properties:
      description:
        type: string
      url:
        type: string
    required:
      - url
    additionalProperties: false
