module.exports = {
  pathCasing: require('./path-casing'),
  schemaEnumeration: require('./schema-enumeration'),
}
