const { casing } = require('@stoplight/spectral-functions');

module.exports = function (input, options) {
  const segments = input
    .split('/')                    // path separator is always a slash
    .filter(s => s !== '')         // filter empty segments due to leading/trailing slash
    .filter(s => {                 // remove path parameter segments
      return !(s.startsWith('{') && s.endsWith('}'))
    })
    .map(s => {                    // segments should match the defined casing in type
      return casing(s, { type: options.type })
    })
    .every(v => v === undefined);  // no segment should report back error

  if (!segments) {
    return [ { message: `must be ${options.type} case` }];
  }

  return;
};
