const yaml = require('js-yaml');
const fs = require('fs');

let cache = new Map();

module.exports = function (input, options, context) {
  if (!cache.has(options.schema)) {
    let document;

    if (options.file) {
      // load document from file if needed
      try {
        document = yaml.load(fs.readFileSync(options.file, 'utf8'));
      }
      catch (e) {
        throw Error(`file ${options.file} not found`);
      }
    } else {
      // otherwise get the current resolved document
      document = context.documentInventory.resolved;
    }

    if (!(document && document.components && document.components.schemas && document.components.schemas[options.schema])) {
      throw Error(`schema ${options.schema} not found`);
    }

    if (!document.components.schemas[options.schema].enum) {
      throw Error(`schema ${options.schema} must be an enumeration`);
    }

    cache.set(options.schema, document.components.schemas[options.schema].enum);
  }

  if (!cache.get(options.schema).includes(input)) {
    return [ { message: `\`${input}\` should be a valid entry from the ${options.schema} enum` } ];
  }

  return;
};
