# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

## CLI Dependencies

The following dependencies are used as CLI scripts both on CI machines and running locally:

* `@stoplight/spectral`
* `@stoplight/prism-cli`
* `@redocly/cli`

On updating these packages in `package.json`, please make sure to update the respective versions in `Dockerfile` as well.
