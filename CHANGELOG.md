# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [16.3.1](https://gitlab.com/team-supercharge/oasg/-/compare/v16.3.0...v16.3.1) (2025-03-05)


### Build

* **deps:** allow newer than 10.0.0 versions of nestjs as peer dep ([418ece8](https://gitlab.com/team-supercharge/oasg/-/commit/418ece8eadb28037caa52d995bc23534a9388783))

## [16.3.0](https://gitlab.com/team-supercharge/oasg/-/compare/v16.2.0...v16.3.0) (2025-02-11)


### Features

* add support for webhook merging and source processing ([6173ff9](https://gitlab.com/team-supercharge/oasg/-/commit/6173ff9a4bf923674c5f5ae3b5c52cb47c113024))
* **angular:** upgrade generator from 7.0.1 to 7.11.0 ([e5f5989](https://gitlab.com/team-supercharge/oasg/-/commit/e5f5989a91f294bd12637623b4f5141c034c8b3a))
* **spring-kotlin, feign-kotlin:** upgrade generators from 7.0.1 to 7.11.0 ([bd7679d](https://gitlab.com/team-supercharge/oasg/-/commit/bd7679dd84e8af854f2d9ede4fb1a87e10c84d70))
* **spring, feign:** upgrade generators from 7.0.1 to 7.11.0 ([83215b2](https://gitlab.com/team-supercharge/oasg/-/commit/83215b2a1a1b797245338925b9a12c8934105df3))


### Bug Fixes

* only perform webhook merging if webhooks are present ([a78c0c8](https://gitlab.com/team-supercharge/oasg/-/commit/a78c0c8dd0749970605a83e1d498c29b8e0010bb))


### Build

* **deps:** update @apidevtools/swagger-parser to 10.1.1 ([c2f2876](https://gitlab.com/team-supercharge/oasg/-/commit/c2f2876dd1484e7a1e777acc1ec13cc51e4c9e51))


### Documentation

* mark affected targets with new features ([8c8825f](https://gitlab.com/team-supercharge/oasg/-/commit/8c8825fb52255d350c27976d67c51945cdbda5d5))

## [16.2.0](https://gitlab.com/team-supercharge/oasg/-/compare/v16.1.0...v16.2.0) (2025-02-07)


### Features

* **python:** re-introduce python client generator with Pydantic v2 ([1e95220](https://gitlab.com/team-supercharge/oasg/-/commit/1e95220d046f42cd8693de0020ffe9c6528d08db))

## [16.1.0](https://gitlab.com/team-supercharge/oasg/-/compare/v16.0.0...v16.1.0) (2025-02-06)


### Features

* **typescript-axios:** introduce typescript-axios client generator ([d842439](https://gitlab.com/team-supercharge/oasg/-/commit/d842439d6b200be06c4ca0b7124e2f2f5365fcc5))
* **typescript-fetch:** introduce typescript-fetch client generator ([b29cb41](https://gitlab.com/team-supercharge/oasg/-/commit/b29cb41eeb256fee0c4e9eb97af3699ebff445d3))


### Build

* **deps:** require at least node 20.17.0 to match spectral's requirements ([453dcba](https://gitlab.com/team-supercharge/oasg/-/commit/453dcbac86768cce1e8fa43e6cfe9eb11555beb8)), closes [/github.com/stoplightio/spectral/blob/v6.14.1/package.json#L25](https://gitlab.com/team-supercharge/oasg/-/issues/L25)
* **deps:** update ajv to latest version ([40ec650](https://gitlab.com/team-supercharge/oasg/-/commit/40ec650a6327a0b68c508b0ac0d7fd8d2d3b5627)), closes [#25](https://gitlab.com/team-supercharge/oasg/-/issues/25)


### Documentation

* mark bugfixing for linter at version 16 ([57aa719](https://gitlab.com/team-supercharge/oasg/-/commit/57aa71913006b243618cee01ccf9e64c2cded742))

## [16.0.0](https://gitlab.com/team-supercharge/oasg/-/compare/v15.1.1...v16.0.0) (2025-02-06)


### ⚠ BREAKING CHANGES

* **nestjs:** improve auth guard template
* **python-legacy:** rename python target to python-legacy

### Features

* **nestjs:** improve auth guard template ([1786217](https://gitlab.com/team-supercharge/oasg/-/commit/1786217779a7276aa3d4e297ab1d2a1d76a562ec))
* **python-legacy:** rename python target to python-legacy ([53227b1](https://gitlab.com/team-supercharge/oasg/-/commit/53227b161b75bf2a900e943165d309a1046ecc3a))


### Documentation

* add version 16 to history and to the migration guide ([77d995d](https://gitlab.com/team-supercharge/oasg/-/commit/77d995d99526c52f07ba828d841504277f0faba3))

### [15.1.1](https://gitlab.com/team-supercharge/oasg/-/compare/v15.1.0...v15.1.1) (2025-01-31)


### Bug Fixes

* **react-query:** remove unnecessary destructuring from mutation options ([437d9b9](https://gitlab.com/team-supercharge/oasg/-/commit/437d9b9a71eb735658407fa12a4a35a966ed0eb4))


### Documentation

* mark react with bugfix for v15 ([42c7db2](https://gitlab.com/team-supercharge/oasg/-/commit/42c7db288583cee2a398858011a19730e883b8a4))

## [15.1.0](https://gitlab.com/team-supercharge/oasg/-/compare/v15.0.0...v15.1.0) (2025-01-31)


### Features

* **nestjs:validations:** adds custom validator to handle date only ISO dates ([3f0359d](https://gitlab.com/team-supercharge/oasg/-/commit/3f0359d4f1c02adc16fd811d2f97159ad8cf3a44))

## [15.0.0](https://gitlab.com/team-supercharge/oasg/-/compare/v14.1.0...v15.0.0) (2025-01-09)


### ⚠ BREAKING CHANGES

* **nestjs:** adds authentication guard by endpoints
* **nestjs:** drops nest v9 support

### Features

* **nestjs:model:** extends validation capabilities ([9f182f6](https://gitlab.com/team-supercharge/oasg/-/commit/9f182f603d9e967a3839758cf572a02a5232a1ca))
* **nestjs:security-scheme:** exports enum from root index.ts ([6b5ecca](https://gitlab.com/team-supercharge/oasg/-/commit/6b5eccabf42599a09ae295d722fb885ecc798835))
* **nestjs:** add security schemes as enum ([2c9dffc](https://gitlab.com/team-supercharge/oasg/-/commit/2c9dffc57ea602add38652bbde283b46611ff38a))
* **nestjs:** adds authentication guard by endpoints ([f2de0f1](https://gitlab.com/team-supercharge/oasg/-/commit/f2de0f142a0f4dab346b859ff58bf8befecddf02))
* **targets:** postman collection ([bbb8161](https://gitlab.com/team-supercharge/oasg/-/commit/bbb8161f7d8841faf9b352cdb83b45a8fb63400e))


### Bug Fixes

* **nestjs:api.service:** cleanup formatting ([70d1ff2](https://gitlab.com/team-supercharge/oasg/-/commit/70d1ff291458ede6b2f1f3be4a679b7cab44e91e))
* **nestjs:auth-guard:** export security-scheme enum from templated auth-guard ([480a8d3](https://gitlab.com/team-supercharge/oasg/-/commit/480a8d36ba52c471b7fd2e7163d2dfa8b3c79709))
* **nestjs:model-generic:** missing `{` and formatting issues ([38a362c](https://gitlab.com/team-supercharge/oasg/-/commit/38a362cd2401ea54c649d05913889db46a4aab04))
* **nest:** move RESOLVE_INLINE_ENUMS option to default config ([b4ca161](https://gitlab.com/team-supercharge/oasg/-/commit/b4ca161b015c7ee02a80a7e556626845cb9e9aae))


### Chores

* **nestjs:** drops nest v9 support ([6c5ba50](https://gitlab.com/team-supercharge/oasg/-/commit/6c5ba5095f4b61030e00b97a07b8f73338144158))


### CI

* increase test job pod size to xl ([3cfa7fd](https://gitlab.com/team-supercharge/oasg/-/commit/3cfa7fd5a6d81335077602213ad6be630d8da00a))


### Styling

* **nestjs:model:** format decorators into a more readable state ([0ff2225](https://gitlab.com/team-supercharge/oasg/-/commit/0ff22259de5a0ec60d48136d6c8415fc91a9a9ad))


### Build

* **deps:** update vulnerable dependencies ([43273ed](https://gitlab.com/team-supercharge/oasg/-/commit/43273eddf41eece131d832a05c4c2d81d6879c66))


### Documentation

* add 14 => v15 section to migration guide ([45c0c0a](https://gitlab.com/team-supercharge/oasg/-/commit/45c0c0a04f1a693ce7c309e43e8d80b72bd3617f))
* add RFC for dependency version locking in Node.js-based targets ([816dbf2](https://gitlab.com/team-supercharge/oasg/-/commit/816dbf2a84ff4d0f12a7407d214a5473e8ac51c4))
* add version overview in table format ([610cbf2](https://gitlab.com/team-supercharge/oasg/-/commit/610cbf20c2bbf6704a5c3e467f25f89a37bde78d))
* **nestjs:** add info about inline schemas ([0e4b258](https://gitlab.com/team-supercharge/oasg/-/commit/0e4b25840a23d3e7fb4d5bd6c810813ad56b6423))
* **nestjs:** redo validation table to be based on open api spec ([318b10e](https://gitlab.com/team-supercharge/oasg/-/commit/318b10e7fdd3894ccf94b77b30ba87ca3aeea2c1))
* **readme:** fix spelling errors ([1f5dd29](https://gitlab.com/team-supercharge/oasg/-/commit/1f5dd2909822559c9a3e8b89f8c97db853f61ef5))


### Testing

* **nestjs:** reenable and fix nestjs generation testcase ([6d26ade](https://gitlab.com/team-supercharge/oasg/-/commit/6d26aded8c4dd20baf8b8df8ead3f5383bb0e820))
* **nestjs:** update snapshots for e2e test ([0a2061b](https://gitlab.com/team-supercharge/oasg/-/commit/0a2061b6eaad8cca708a552056eaa9b2a74ee246))

## [14.1.0](https://gitlab.com/team-supercharge/oasg/compare/v14.0.1...v14.1.0) (2024-12-03)


### Features

* add python-fastapi target ([b1042ad](https://gitlab.com/team-supercharge/oasg/commit/b1042ad9e9594bce6d71d8c517dd3d3716c25ba1))
* add unmodified python fastapi generator from 7.8.0 ([e25c0a0](https://gitlab.com/team-supercharge/oasg/commit/e25c0a081ceced88ecb8886da14e7c813e37033a))
* **kmp:** add Kotlin Multiplatform client generator ([c83c116](https://gitlab.com/team-supercharge/oasg/commit/c83c116984181a320e7b5e17f0bb866b514c94bb))
* support windows ([ffd21d9](https://gitlab.com/team-supercharge/oasg/commit/ffd21d9ffff9793282ef85b28ab6ab14225eb2a1))
* **target:** dotnet ([2e109de](https://gitlab.com/team-supercharge/oasg/commit/2e109dee32fafec66878e812c1c5c74f5c540621))
* update python-fastapi target templates ([2647d59](https://gitlab.com/team-supercharge/oasg/commit/2647d5918f9d864782a5f9282df8cdd5d9c42ca0))
* use PEP 440 compatible versioning with python generators ([e9b0f3e](https://gitlab.com/team-supercharge/oasg/commit/e9b0f3e53e26b299f9fcfaa459913ecda3bd3c47))


### Bug Fixes

* **android:** fix publishing group id ([998336b](https://gitlab.com/team-supercharge/oasg/commit/998336b0b7a670fe3450d0c548bc197e62cfaa63))
* **angular:** override @types/node version in the angular generator manually ([2a12ad1](https://gitlab.com/team-supercharge/oasg/commit/2a12ad1ba0e948bd6089c3afbca0af1cb87904c9))
* lock importlib_metadata version ([109bfb2](https://gitlab.com/team-supercharge/oasg/commit/109bfb2c53854481919468978ded87e595cabe8e))


### Testing

* skip spring-simple ([aa56b6a](https://gitlab.com/team-supercharge/oasg/commit/aa56b6ae25f17b1820d34f38bb18ee8987b075e2))


### Documentation

* **nestjs:** class-validator limitations ([25dd52f](https://gitlab.com/team-supercharge/oasg/commit/25dd52facdec47d856ba045b7a1458056b03e539))

### [14.0.1](https://gitlab.com/team-supercharge/oasg/compare/v14.0.0...v14.0.1) (2024-08-28)


### Bug Fixes

* **python:** system-wide install of twine module ([bc7042c](https://gitlab.com/team-supercharge/oasg/commit/bc7042ce75b472abb5b8c2c98827b369105bf98f))

## [14.0.0](https://gitlab.com/team-supercharge/oasg/compare/v13.1.3...v14.0.0) (2024-08-08)


### ⚠ BREAKING CHANGES

* use node 20.15.0 LTS for the project

### Features

* upgrade cli packages, swagger-ui-express and vulnerable packages ([e727255](https://gitlab.com/team-supercharge/oasg/commit/e727255aad110a41e3a9a18786a549d8823553f7))


### Build

* use node 20.15.0 LTS for the project ([0bdfc89](https://gitlab.com/team-supercharge/oasg/commit/0bdfc89a086101f8b7aea56b65c3e0b7f7305062))


### Testing

* enable e2e tests for python, contract, api-docs, feign, flutter ([60f1dae](https://gitlab.com/team-supercharge/oasg/commit/60f1daee8a9085543fea729d46cd1e05f830fa1a))
* fix duplicate package name for angular generators ([f5c3cd4](https://gitlab.com/team-supercharge/oasg/commit/f5c3cd4c14a26bec6143ac0ff3757ce8f384d820))
* run angular e2e tests with default ngVersion, currently 16.0.0 ([be2b24b](https://gitlab.com/team-supercharge/oasg/commit/be2b24b14a57b25908bf23a37932a40214b7f702))


### Documentation

* add migration information about node version requirements ([48a2f2d](https://gitlab.com/team-supercharge/oasg/commit/48a2f2d1d42d0bf9a901232215ad73547568169f))

### [13.1.3](https://gitlab.com/team-supercharge/oasg/compare/v13.1.2...v13.1.3) (2024-05-13)


### Bug Fixes

* **nestjs:** object.keys expects object type for isEnum ([43cd873](https://gitlab.com/team-supercharge/oasg/commit/43cd873770380de85873fc181d255a21361f3365))

### [13.1.2](https://gitlab.com/team-supercharge/oasg/compare/v13.1.1...v13.1.2) (2024-01-10)


### Bug Fixes

* rewrite nest path parameters ([a3fc9ff](https://gitlab.com/team-supercharge/oasg/commit/a3fc9ff357e8d7961f65703de494aa3f167b0829))

### [13.1.1](https://gitlab.com/team-supercharge/oasg/compare/v13.1.0...v13.1.1) (2024-01-10)


### Bug Fixes

* nest class validator types ([511c03c](https://gitlab.com/team-supercharge/oasg/commit/511c03c9b87cd23f712e704ed4ecb8d02168cd41))

## [13.1.0](https://gitlab.com/team-supercharge/oasg/compare/v13.0.1...v13.1.0) (2024-01-10)


### Features

* add react query hooks ([1fc0964](https://gitlab.com/team-supercharge/oasg/commit/1fc096409b3f7b7d01a808d4a054132c4560da4a))
* **react:** add abort signal to hooks ([047282f](https://gitlab.com/team-supercharge/oasg/commit/047282fa123193c91620ba22ba009b481b10b997))
* **react:** add api configuration helper hooks ([d9ef666](https://gitlab.com/team-supercharge/oasg/commit/d9ef66636cca018e6ed176fc8aa882fedc3ef144))
* **react:** add default enabled hook option ([6bb915f](https://gitlab.com/team-supercharge/oasg/commit/6bb915fcde6fabd82658fef1df70d86893730aa4))
* **react:** add mutation context to hooks ([ac68fc1](https://gitlab.com/team-supercharge/oasg/commit/ac68fc1d08bed4f87681e2a05b8562fbeecd54d5))

### [13.0.1](https://gitlab.com/team-supercharge/oasg/compare/v13.0.0...v13.0.1) (2023-10-04)


### Documentation

* update readme with missing 13.0.0 version ([f575abc](https://gitlab.com/team-supercharge/oasg/commit/f575abc757ae69eb3df47017d1da9480e2fb2dd3))

## [13.0.0](https://gitlab.com/team-supercharge/oasg/compare/v12.2.0...v13.0.0) (2023-10-04)


### ⚠ BREAKING CHANGES

* **android:** use same inline schema options as other generators
* **react:** rename and update target to openapi-generator 7.0.1
* **nestjs:** update target to openapi-generator 7.0.1
* **ios:** update ios target to openapi-generator 7.0.0
* **python:** update python target to openapi-generator 7.0.0
* **angular:** update angular target to openapi-generator 7.0.0

### Features

* **android:** update ktlint formatter ([ec3d20b](https://gitlab.com/team-supercharge/oasg/commit/ec3d20bdedff22d27b0c0368c56c43f4b97e1665))
* **android:** update OpenAPI Generator version ([22a10db](https://gitlab.com/team-supercharge/oasg/commit/22a10db37506285af22cb8f83e05106176e53373))
* **android:** use same inline schema options as other generators ([83ca795](https://gitlab.com/team-supercharge/oasg/commit/83ca795dc2d4d8ce03aacfa6f158ebe4e9059b34))
* **angular:** update angular target to openapi-generator 7.0.0 ([944f9e2](https://gitlab.com/team-supercharge/oasg/commit/944f9e253d6271ec335cd23bc8af12c6a5aca822))
* **flutter:** add flutter target with dart-dio generator ([07cf46b](https://gitlab.com/team-supercharge/oasg/commit/07cf46b044ec1a89b62bc841bf705493720eba55))
* **flutter:** update target to openapi-generator 7.0.1 ([55ab760](https://gitlab.com/team-supercharge/oasg/commit/55ab7609b3ec25fbe794efeb240a07767825b780))
* **ios:** update ios target to openapi-generator 7.0.0 ([2e68287](https://gitlab.com/team-supercharge/oasg/commit/2e682873ec07f12402ea23b11f7db909957b452b))
* **ios:** update iOS target to openapi-generator 7.0.1 ([c18743f](https://gitlab.com/team-supercharge/oasg/commit/c18743f596b550882857b24eab37078c39b4d09f))
* **nestjs:** update target to openapi-generator 7.0.1 ([fed85a8](https://gitlab.com/team-supercharge/oasg/commit/fed85a8e9fdb66a0343299fb5d869325f268c5be))
* **python:** update python target to openapi-generator 7.0.0 ([5d93727](https://gitlab.com/team-supercharge/oasg/commit/5d9372767a7b97017cd5e79b7641d7cc645958a0))
* **react:** rename and update target to openapi-generator 7.0.1 ([a1628b9](https://gitlab.com/team-supercharge/oasg/commit/a1628b9c2a1412d9cbe0c73fff9ffc9266327d96))
* **spring-kotlin, feign-kotlin:** upgrade targets to openapi-generator 7.0.1 ([c279868](https://gitlab.com/team-supercharge/oasg/commit/c2798689badc24c569a8778fcbd6c15e5b576eb6))
* **spring, feign:** upgrade targets to openapi-generator 7.0.1 ([4142a68](https://gitlab.com/team-supercharge/oasg/commit/4142a681739152f27ae66d3d637e7063198eacfd))
* update redocly cli to 1.2.0 stable version ([5d5d4c8](https://gitlab.com/team-supercharge/oasg/commit/5d5d4c8af051a2dbf0ed9e1fea0c9266f2e4ea7c))


### Bug Fixes

* **angular:** update target to openapi-generator 7.0.1 ([4734bf7](https://gitlab.com/team-supercharge/oasg/commit/4734bf746a635de9981005018793dde7459f5bc4))
* **feign-kotlin:** adjust template override placement ([f21b583](https://gitlab.com/team-supercharge/oasg/commit/f21b5830c2a7b8c05cb6f6fc85f49b91b680229f))
* **flutter, swift:** remove hardcoded branch name ([a5f7125](https://gitlab.com/team-supercharge/oasg/commit/a5f71251dbb8b7a0623f5a43deead1b67a8d68b1))
* **flutter:** define docker ENV variables ([b250d29](https://gitlab.com/team-supercharge/oasg/commit/b250d297df94f74fa19c08a63ac52ac2d8f8decc))
* **nestjs:** do not add RequiredPipe on Headers annotation ([39743c1](https://gitlab.com/team-supercharge/oasg/commit/39743c168e6f1046f9a4ea2503489bdd862808d5))
* **python:** update target to openapi-generator 7.0.1 ([85942e3](https://gitlab.com/team-supercharge/oasg/commit/85942e3dc744eca1177a0bbead49b75531e23f24))


### Refactoring

* default version and generator mapping for targets more readable ([e372b5c](https://gitlab.com/team-supercharge/oasg/commit/e372b5cc1d9ae08e3f4e985965f41e87827c6ecd))


### Documentation

* fix typo in README ([8b6cd0d](https://gitlab.com/team-supercharge/oasg/commit/8b6cd0d760a2f02759dc2504c3da65e706c7fe0a))

## [12.2.0](https://gitlab.com/team-supercharge/oasg/compare/v12.1.2...v12.2.0) (2023-07-26)


### Features

* **nestjs:** add NestJS 10 support to peerDependencies ([b794946](https://gitlab.com/team-supercharge/oasg/commit/b7949468f98639a17e8b27ce42a4bf01f3a3d040))


### Bug Fixes

* **nestjs:package:** update class-validator to 0.14.0 fixing audit issue ([ed020bc](https://gitlab.com/team-supercharge/oasg/commit/ed020bcc4d6e1d4b8ecb53cc1b33508702c5c49c))
* **nestjs:required-pipe:** explicit definedness check on input. Ref [#19](https://gitlab.com/team-supercharge/oasg/issues/19) ([0211ac5](https://gitlab.com/team-supercharge/oasg/commit/0211ac52303816e80a50957fde220aa363be66d3))

### [12.1.2](https://gitlab.com/team-supercharge/oasg/compare/v12.1.1...v12.1.2) (2023-07-25)


### Bug Fixes

* **nestjs:** add nestjs streamablefile return type ([30ef4c3](https://gitlab.com/team-supercharge/oasg/commit/30ef4c32bcb4897156dc9bc45798ca86c82604fe))
* **swift:** handle preRelease version and naming convention ([5776bca](https://gitlab.com/team-supercharge/oasg/commit/5776bcade976b1e3032dc2601f48c2a3a9085694))

### [12.1.1](https://gitlab.com/team-supercharge/oasg/compare/v12.1.0...v12.1.1) (2023-06-16)


### Build

* set correct repo URL in package.json ([37ecc8b](https://gitlab.com/team-supercharge/oasg/commit/37ecc8b34106a8c48bb7e45b80797cda785b7eef))

## [12.1.0](https://gitlab.com/team-supercharge/oasg/compare/v12.0.1...v12.1.0) (2023-06-05)


### Features

* **python:** update to latest generator to fix description content type bug ([f01df3b](https://gitlab.com/team-supercharge/oasg/commit/f01df3b9cf87f4211504ff08cf85802d619369b5))

### [12.0.1](https://gitlab.com/team-supercharge/oasg/compare/v12.0.0...v12.0.1) (2023-06-05)


### Bug Fixes

* prepare for non-http method path keys (parameters. description, etc) ([7551f66](https://gitlab.com/team-supercharge/oasg/commit/7551f66aeaee6aa785c78a5078074c2c94d78212))
* preprare for non-existing paths and components.schemas ([78753c5](https://gitlab.com/team-supercharge/oasg/commit/78753c5876627ed3d460e1ff49e5ba0e5a0533b3))


### CI

* enable hotfix releases ([9166416](https://gitlab.com/team-supercharge/oasg/commit/9166416f1a45965340571988f3f71f5f5f560ea8))


### Documentation

* update 10 -> 11 migration guide with JS ruleset migration steps ([2f03459](https://gitlab.com/team-supercharge/oasg/commit/2f0345999fd31aea028018c7c35337be9a0d93ae))

## [12.0.0](https://gitlab.com/team-supercharge/oasg/compare/v11.0.0...v12.0.0) (2023-06-02)


### ⚠ BREAKING CHANGES

* move spec preprocessing steps from openapi target to source configuration

### Features

* move spec preprocessing steps from openapi target to source configuration ([c9a7b58](https://gitlab.com/team-supercharge/oasg/commit/c9a7b58aab0987c077eb9a8fc18c9c427e841ca2))


### Bug Fixes

* add individual glob results sorted alphabetically to input list ([1d193c9](https://gitlab.com/team-supercharge/oasg/commit/1d193c9136a9e3b9b48b7e648c9c17f48c49f5b5))
* improve logging for merge, display input and output files ([9626ba9](https://gitlab.com/team-supercharge/oasg/commit/9626ba98ab4b58773a1943cc982d845ab27eae9d))


### CI

* check building docker image on merge requests ([b7d87d3](https://gitlab.com/team-supercharge/oasg/commit/b7d87d35899ec6aefe8b465e4f7ef7072a93cf3c))

## [11.0.0](https://gitlab.com/team-supercharge/oasg/compare/v10.1.0...v11.0.0) (2023-05-19)


### ⚠ BREAKING CHANGES

* use updated Spectral 6.6.0 dependency for linting specs

### Features

* check for version mismatch between package.json and actual ([ef0f771](https://gitlab.com/team-supercharge/oasg/commit/ef0f77109c181467ba77c3181b83a479a25ceab4))
* use updated Spectral 6.6.0 dependency for linting specs ([1f89be6](https://gitlab.com/team-supercharge/oasg/commit/1f89be6fe19b1bab90ff12fb7003f632143f56dd))


### CI

* build release docker image on larger machine ([3e5fd63](https://gitlab.com/team-supercharge/oasg/commit/3e5fd638df3100f0d7c41af9c5d0379100edc906))


### Testing

* update mock directory ([d0d486f](https://gitlab.com/team-supercharge/oasg/commit/d0d486f49c5101b580b3208f63409dabdced4f33))
* use openapi 3.0.2 schema for testing instead of swagger 2.0 ([1e55c0d](https://gitlab.com/team-supercharge/oasg/commit/1e55c0d431ddbc9ad1b2edc1be6e8cb4b9f32677))


### Build

* explicitly set files to include in published npm package ([4038cdc](https://gitlab.com/team-supercharge/oasg/commit/4038cdca0e5ad47c26c3d35f221173c672395a7a))
* update minor project dependencies ([2a1b833](https://gitlab.com/team-supercharge/oasg/commit/2a1b833738d8bdd8875d95d5bd721018545cae98))


### Documentation

* improve README structure ([7a07d2d](https://gitlab.com/team-supercharge/oasg/commit/7a07d2dd6f49cbac7560fcf54b52e425459fb42b))

## [10.1.0](https://gitlab.com/team-supercharge/oasg/compare/v10.0.2...v10.1.0) (2023-05-19)


### Features

* **android:** update OpenAPI Generator to 6.6.0 ([5c9c730](https://gitlab.com/team-supercharge/oasg/commit/5c9c730e473b8e00033dd987be7384331c4604df))
* **spring-kotlin, feign:** add attach sources to pom.mustache files ([aef7e33](https://gitlab.com/team-supercharge/oasg/commit/aef7e33abc34f59af6b3eea237c363fbfc226293))
* **spring-kotlin, feign:** add Kotlin based Spring Cloud OpenFeign generator ([f8f1291](https://gitlab.com/team-supercharge/oasg/commit/f8f1291f1f28d641960030716cd4042e55bf8586))
* **spring, spring-kotlin, feign:** update OpenApi Generator to 6.6.0 ([05a203b](https://gitlab.com/team-supercharge/oasg/commit/05a203b3a781a7afd82859a379e02f5fe426f421))


### Bug Fixes

* **spring-kotlin, feign:** fix left in @RequestMapping in the apiInterface.mustache ([7e44653](https://gitlab.com/team-supercharge/oasg/commit/7e44653b74a37231dbe6ae58595f976480316e11))

### [10.0.2](https://gitlab.com/team-supercharge/oasg/compare/v10.0.1...v10.0.2) (2023-05-16)


### Bug Fixes

* **android:** remove Response wrapper from function return types ([ee56a6a](https://gitlab.com/team-supercharge/oasg/commit/ee56a6a3416eac2d533d46b8152235e4291b868f))

### [10.0.1](https://gitlab.com/team-supercharge/oasg/compare/v10.0.0...v10.0.1) (2023-05-10)


### Bug Fixes

* **android:** fix Gitlab publishing from CI ([e0bc1cf](https://gitlab.com/team-supercharge/oasg/commit/e0bc1cfe25917a89b1933b1f13b23c0e85e46b0c))

## [10.0.0](https://gitlab.com/team-supercharge/oasg/compare/v9.2.3...v10.0.0) (2023-05-05)


### ⚠ BREAKING CHANGES

* **android:** 'projectName' parameter was removed; 'groupId' and 'artifactId' were added
as required parameter for android targets.

### Features

* **android:** update android target to use latest openapi-generator ([ab816ba](https://gitlab.com/team-supercharge/oasg/commit/ab816ba628ace75e342817fc7e3d5d6199298202))


### Documentation

* extend documentation on Docker-based usage ([d92c9c0](https://gitlab.com/team-supercharge/oasg/commit/d92c9c01eea801527873c265f06fe93bdc17dc5b))

### [9.2.3](https://gitlab.com/team-supercharge/oasg/compare/v9.2.2...v9.2.3) (2023-05-05)


### Bug Fixes

* keep original sorting for merged input ([a8ae000](https://gitlab.com/team-supercharge/oasg/commit/a8ae000faf1f8cc69c6b2e55a089bae11841c309))


### CI

* add e2e tests ([cca550c](https://gitlab.com/team-supercharge/oasg/commit/cca550c06e7f15e65262ad5829efc45178368c48))


### Testing

* add e2e tests ([4e21e1a](https://gitlab.com/team-supercharge/oasg/commit/4e21e1a4b4aa462347e026adc9340e227726d9b2))
* add merged e2e test ([603cceb](https://gitlab.com/team-supercharge/oasg/commit/603cceb0483a1492e07bdca20466e6b69079be19))
* add more type to e2e ([cd506b1](https://gitlab.com/team-supercharge/oasg/commit/cd506b1a06d6a454899c70b90002ed9d174ec53f))

### [9.2.2](https://gitlab.com/team-supercharge/oasg/compare/v9.2.1...v9.2.2) (2023-04-14)


### Bug Fixes

* **angular:** run the npm publish in the right folder ([be32f40](https://gitlab.com/team-supercharge/oasg/commit/be32f40fffbc3f6b080c98adcca809657165b4dc))
* explicitly install deps to Docker-embedded oasg ([fb83ece](https://gitlab.com/team-supercharge/oasg/commit/fb83ecea2c8756dab942284f014fdcd695529719))

### [9.2.1](https://gitlab.com/team-supercharge/oasg/compare/v9.2.0...v9.2.1) (2023-04-13)


### Bug Fixes

* **nestjs:** use node for post-processing files instead of sed ([22d996b](https://gitlab.com/team-supercharge/oasg/commit/22d996bd0c958108871bc8d331a44aa4f72f99b7))


### CI

* upgrade to latest jarvis ([c444628](https://gitlab.com/team-supercharge/oasg/commit/c4446285ce4a97e6fd75239be27cb08b054f9f68))
* use latest lts node version ([bdd39a6](https://gitlab.com/team-supercharge/oasg/commit/bdd39a663a5d8c1f37704c9ad0c12148dd217f6f))

## [9.2.0](https://gitlab.com/team-supercharge/oasg/compare/v9.1.1...v9.2.0) (2023-04-03)


### Features

* add glob patterns to merger ([e7ea3bd](https://gitlab.com/team-supercharge/oasg/commit/e7ea3bdc28958e3a8ea4de7af10a1cac48645b5b))

### [9.1.1](https://gitlab.com/team-supercharge/oasg/compare/v9.1.0...v9.1.1) (2023-03-06)


### Bug Fixes

* **nestjs:** fix typo in patch request sed command ([0985788](https://gitlab.com/team-supercharge/oasg/commit/0985788d9d6e0cfe542b30603584716f640517f0))

## [9.1.0](https://gitlab.com/team-supercharge/oasg/compare/v9.0.0...v9.1.0) (2023-02-28)


### Features

* **openapi:** add cleanup steps for openapi target ([01626ca](https://gitlab.com/team-supercharge/oasg/commit/01626ca1454be73afb420310503fd6af3b0164f9))

## [9.0.0](https://gitlab.com/team-supercharge/oasg/compare/v8.0.2...v9.0.0) (2023-02-24)


### ⚠ BREAKING CHANGES

* **spring-kotlin, spring, feign:** skip inline schema reusing by default

### Features

* **spring-kotlin, spring, feign:** skip inline schema reusing by default ([c125332](https://gitlab.com/team-supercharge/oasg/commit/c125332cbd59dbd31b2e257060a716950ed3f03a))

### [8.0.2](https://gitlab.com/team-supercharge/oasg/compare/v8.0.1...v8.0.2) (2023-02-24)


### Bug Fixes

* config for inline schema naming, extend migration guide ([e589430](https://gitlab.com/team-supercharge/oasg/commit/e589430a00687c41e725fcef0285cf4af4fb61d6))

### [8.0.1](https://gitlab.com/team-supercharge/oasg/compare/v8.0.0...v8.0.1) (2023-02-23)


### Bug Fixes

* install dependencies used as CLI scripts in Docker ([5d041e0](https://gitlab.com/team-supercharge/oasg/commit/5d041e0c7d1651bd21fcb33287c237b6a18bb6f1))
* use fixed version for prism and redocly ([3986814](https://gitlab.com/team-supercharge/oasg/commit/39868141b55234e922ce8df5dd58d88a0b54baf3))


### Documentation

* improve extending the default oasg ruleset, add to migration notes ([9a70342](https://gitlab.com/team-supercharge/oasg/commit/9a703421a6eb7e75ddb8e83753fd9a6bc3b9edc7))

## [8.0.0](https://gitlab.com/team-supercharge/oasg/compare/v7.1.0...v8.0.0) (2023-02-22)


### ⚠ BREAKING CHANGES

* **feign:** upgrade generator to 6.3.0, migrate to spring boot 3
* **spring:** upgrade generator to 6.3.0, migrate to spring boot 3
* **spring-kotlin:** upgrade generator to 6.3.0, migrate to spring boot 3

### Features

* dockerize oasg with its runtime dependencies ([450d9ee](https://gitlab.com/team-supercharge/oasg/commit/450d9ee8a2f475ac9f1b31810b9b87611ece3035))
* **feign:** optionally attach sources ([f38310a](https://gitlab.com/team-supercharge/oasg/commit/f38310a9903ecc2746ed1cab244c1ac4083ba868))
* **feign:** upgrade generator to 6.3.0, migrate to spring boot 3 ([8a5fb6d](https://gitlab.com/team-supercharge/oasg/commit/8a5fb6d4c2f9cb65be5ec6b828ca0e81188e783a))
* **spring-kotlin:** attach source ([9622b0b](https://gitlab.com/team-supercharge/oasg/commit/9622b0bad2c57385ab9897f931e96e26a07d665b))
* **spring-kotlin:** upgrade generator to 6.3.0, migrate to spring boot 3 ([2bad155](https://gitlab.com/team-supercharge/oasg/commit/2bad155bd0b1c3024bc34176352c0c4d2e646492))
* **spring:** upgrade generator to 6.3.0, migrate to spring boot 3 ([7efa94c](https://gitlab.com/team-supercharge/oasg/commit/7efa94c35fb532b18b0776bad77facea678bbd41))
* **swift:** remove JSONEncoder default outputformatting value ([a11b59a](https://gitlab.com/team-supercharge/oasg/commit/a11b59a62b3c0ded4efa61dad57a6606302bdc00))


### Bug Fixes

* **kotlin-spring:** generate required nullable properties as optional ([d79faf7](https://gitlab.com/team-supercharge/oasg/commit/d79faf781781b0295d075c7a07b927c8c0f7374f))
* **spring-kotlin:** optionally remove spring boot repackage goal ([eee4cf5](https://gitlab.com/team-supercharge/oasg/commit/eee4cf5bb8233dbb0257de70ee3239b7622f681d))

## [7.1.0](https://gitlab.com/team-supercharge/oasg/compare/v7.0.0...v7.1.0) (2023-02-22)


### Features

* merge complete specifications with every possible field ([60b2e61](https://gitlab.com/team-supercharge/oasg/commit/60b2e614fd72238a9b64a77cc975de24f08f75cb))
* **openapi:** add openapi target for outputting documentation ([051da63](https://gitlab.com/team-supercharge/oasg/commit/051da63eee8bf3eede9c5ca84ea130713128596c))
* **openapi:** add option to sort schemas alphabetically ([0611f5e](https://gitlab.com/team-supercharge/oasg/commit/0611f5e2ba6d65bdb78fd8666a98f69c4d3edd8a))
* **openapi:** bundle specifications using @redocly/cli ([d50fd9c](https://gitlab.com/team-supercharge/oasg/commit/d50fd9c5a987c8dfe476b8659710dcbf52557dca))
* use js-yaml to dump YAML in a more nicely formatted way ([8728bcd](https://gitlab.com/team-supercharge/oasg/commit/8728bcd8c3442531290d487571ae1652ebe5b691))


### Refactoring

* handle dumping YAML files centralized ([1d16b05](https://gitlab.com/team-supercharge/oasg/commit/1d16b059d180bb655befe2e0d905017bd94309b4))

## [7.0.0](https://gitlab.com/team-supercharge/oasg/compare/v6.2.1...v7.0.0) (2023-02-21)


### ⚠ BREAKING CHANGES

* **stubby:** add generateWithDocker required config parameter

### Features

* **stubby:** add generateWithDocker required config parameter ([78b09c3](https://gitlab.com/team-supercharge/oasg/commit/78b09c38b0be235c68ca5908cb4c37b7b6e45017))


### Documentation

* add migration guide for 7.0.0 release ([9b5d77b](https://gitlab.com/team-supercharge/oasg/commit/9b5d77b456b0fc523f952b409d5814e2fe4d8fd1))

### [6.2.1](https://gitlab.com/team-supercharge/oasg/compare/v6.2.0...v6.2.1) (2022-12-08)


### Bug Fixes

* **lint:** cache key handling for schema-enumeration ([34bdc2e](https://gitlab.com/team-supercharge/oasg/commit/34bdc2ef5bd589fade22b2541e2ac0093d374860))

## [6.2.0](https://gitlab.com/team-supercharge/oasg/compare/v6.1.0...v6.2.0) (2022-11-28)


### Features

* **ios:** support generatorCustomArgs config parameter ([7a5b227](https://gitlab.com/team-supercharge/oasg/commit/7a5b227ffcf0aac2da38d8daaed9fc02a7abd20e))

## [6.1.0](https://gitlab.com/team-supercharge/oasg/compare/v6.0.0...v6.1.0) (2022-11-22)


### Features

* **nestjs:** pattern matching for model properties ([86c2748](https://gitlab.com/team-supercharge/oasg/commit/86c2748d8c711983a3706141899aa24806fb92f6))
* **nestjs:** throw custom exception for contract validation errors ([a59094f](https://gitlab.com/team-supercharge/oasg/commit/a59094f0a68f4db9c55e7691bbd8e9dac9109609))


### Bug Fixes

* **nestjs:** generate params + body correctly ([3e30d2a](https://gitlab.com/team-supercharge/oasg/commit/3e30d2a67acbb415ab771e66222b83a4acaecebd))

## [6.0.0](https://gitlab.com/team-supercharge/oasg/compare/v5.1.1...v6.0.0) (2022-11-21)


### ⚠ BREAKING CHANGES

* **ios:** add required interface type of generated client
* **ios:** remove outdated pod support

### Features

* **ios:** add required interface type of generated client ([8801024](https://gitlab.com/team-supercharge/oasg/commit/88010245b74200e70add2f7b99e8f97f2a35f403))
* **ios:** remove outdated pod support ([a34ba86](https://gitlab.com/team-supercharge/oasg/commit/a34ba8666230299058d172116b420e72aaceb67c))

### [5.1.1](https://gitlab.com/team-supercharge/oasg/compare/v5.1.0...v5.1.1) (2022-11-07)


### Bug Fixes

* **feign:** fix bugs in beanValidation & formParams templates ([bfb8a67](https://gitlab.com/team-supercharge/oasg/commit/bfb8a67552d1536f57399281c2405cc5bbdee195))

## [5.1.0](https://gitlab.com/team-supercharge/oasg/compare/v5.0.2...v5.1.0) (2022-11-05)


### Features

* **nestjs:** implement basic nestjs server SDK generation ([c693d46](https://gitlab.com/team-supercharge/oasg/commit/c693d465e5c2f8cd6a142beb5af01f250ad6373c))
* **nestjs:** initialize new nestjs (server) target from angular ([67c5fef](https://gitlab.com/team-supercharge/oasg/commit/67c5fefa8d764dabba5f72661ad5de1926e02c60))


### CI

* update to jarvis@8.0.0 ([30ab59f](https://gitlab.com/team-supercharge/oasg/commit/30ab59f83ea2081c3d0ebcff19ca9c8d96a169d6))

### [5.0.2](https://gitlab.com/team-supercharge/oasg/compare/v5.0.1...v5.0.2) (2022-10-10)


### Bug Fixes

* **angular:** add UPPERCASE enum prop naming to be consistent with other targets ([27baa2c](https://gitlab.com/team-supercharge/oasg/commit/27baa2c9b8ddb7594c6aecb21c81d7e0dfb36b41))

### [5.0.1](https://gitlab.com/team-supercharge/oasg/compare/v5.0.0...v5.0.1) (2022-10-07)


### Bug Fixes

* **angular:** bump default generator version from 4.3.1 to 5.4.0 ([9d2e74b](https://gitlab.com/team-supercharge/oasg/commit/9d2e74bfcb3845c19c9913d41f27e44eb220fd69))

## [5.0.0](https://gitlab.com/team-supercharge/oasg/compare/v4.6.2...v5.0.0) (2022-10-07)


### ⚠ BREAKING CHANGES

* **angular:** remove hard coded typescript version
* customization of generator templates for every target type

### Features

* add mustache templates for spring-kotlin@5.4.0 ([c3b3748](https://gitlab.com/team-supercharge/oasg/commit/c3b37487e76f53d4642146122e4889142eb86957))
* **angular:** remove hard coded typescript version ([c1011e0](https://gitlab.com/team-supercharge/oasg/commit/c1011e03fea752aa73762de4d298f951737d92ce))
* customization of generator templates for every target type ([ad44086](https://gitlab.com/team-supercharge/oasg/commit/ad440869f4575e0ab077777b88eaf4f276ed858f))
* customize templates for fixing spring-kotlin behaviour at 5.4.0 ([82f1ef3](https://gitlab.com/team-supercharge/oasg/commit/82f1ef3cdd15bae0c055a2a6d369aacbb4b9a086))

### [4.6.2](https://gitlab.com/team-supercharge/oasg/compare/v4.6.1...v4.6.2) (2022-10-06)


### Bug Fixes

* **spring-kotlin:** fix Resource to MultipartFile mappings ([0e67ee3](https://gitlab.com/team-supercharge/oasg/commit/0e67ee3c6c3f79f95a4a09378fc884b0de15a00e))

### [4.6.1](https://gitlab.com/team-supercharge/oasg/compare/v4.6.0...v4.6.1) (2022-09-20)


### Bug Fixes

* ajv cli warnings about strict validation ([40089be](https://gitlab.com/team-supercharge/oasg/commit/40089bec3c977292eaf6178c2c4f4d709281fdb2))

## [4.6.0](https://gitlab.com/team-supercharge/oasg/compare/v4.5.1...v4.6.0) (2022-09-20)


### Features

* add schema-enumeration custom linter function ([bd5cd09](https://gitlab.com/team-supercharge/oasg/commit/bd5cd090b7e3c5c85d19c13ee8f4d9f56ba6290c))

### [4.5.1](https://gitlab.com/team-supercharge/oasg/compare/v4.5.0...v4.5.1) (2022-09-14)


### Bug Fixes

* **feign:** fix Date type to LocalDate type import ([3d9e7d7](https://gitlab.com/team-supercharge/oasg/commit/3d9e7d71e2df13275c3544afce9e9c384e5ecb29))

## [4.5.0](https://gitlab.com/team-supercharge/oasg/compare/v4.4.0...v4.5.0) (2022-06-13)


### Features

* turn off removing enum prefixes for feign target ([dbe7809](https://gitlab.com/team-supercharge/oasg/commit/dbe78092f472a10c23762606fd0f1bda8068ac07))

## [4.4.0](https://gitlab.com/team-supercharge/oasg/compare/v4.3.1...v4.4.0) (2022-05-09)


### Features

* set uppercase enum property naming for spring targets ([8898d42](https://gitlab.com/team-supercharge/oasg/commit/8898d42313035edc09308748176ea7b5f393ccca))
* turn off removing enum prefixes for spring and spring-kotlin targets ([db99309](https://gitlab.com/team-supercharge/oasg/commit/db993096cecb7e51b9006e867d9ae19182149559))

### [4.3.1](https://gitlab.com/team-supercharge/oasg/compare/v4.3.0...v4.3.1) (2022-04-27)


### Bug Fixes

* **kotlin-spring:** attach sources jar if exists ([8e60f41](https://gitlab.com/team-supercharge/oasg/commit/8e60f41d0f913a140c505ee9851f345f0bc0cf8a))

## [4.3.0](https://gitlab.com/team-supercharge/oasg/compare/v4.2.0...v4.3.0) (2022-04-27)


### Features

* **kotlin-spring:** adds common generator config ([c38998f](https://gitlab.com/team-supercharge/oasg/commit/c38998fdfc2b09d10c5b54ffdb6a3bd776c44790))
* **kotlin-spring:** attach sources jar if exists ([15f6fc3](https://gitlab.com/team-supercharge/oasg/commit/15f6fc3ee176185d0cf906784fb4429414a738a7))

## [4.2.0](https://gitlab.com/team-supercharge/oasg/compare/v4.1.0...v4.2.0) (2022-04-11)


### Features

* add --preRelease option to generate and publish commands ([f8a8e7c](https://gitlab.com/team-supercharge/oasg/commit/f8a8e7cab31ff6ec56b5e61c502fbe3dc0c16b7b))
* add contract-testing target ([1f41406](https://gitlab.com/team-supercharge/oasg/commit/1f41406e8702dde3baf5669633aa31352347312e))

## [4.1.0](https://gitlab.com/team-supercharge/oasg/compare/v4.0.2...v4.1.0) (2022-03-22)


### Features

* add the ability to setup default generator version by target ([0abbc90](https://gitlab.com/team-supercharge/oasg/commit/0abbc90ba0d4af97c577d76ca1f451f825e3a77f))
* introduce python client generation ([a23e77d](https://gitlab.com/team-supercharge/oasg/commit/a23e77dc9fbf8182e5aac3190a1bcdb28c55a598))

### [4.0.2](https://gitlab.com/team-supercharge/oasg/compare/v4.0.1...v4.0.2) (2022-03-21)


### Bug Fixes

* replace references in mappings of discriminator ([1cc5b67](https://gitlab.com/team-supercharge/oasg/commit/1cc5b67003da1a937b60736f383db78d0524cdc5))

### [4.0.1](https://gitlab.com/team-supercharge/oasg/compare/v4.0.0...v4.0.1) (2022-03-18)


### Bug Fixes

* add missing default generatorId mapping for feign clients ([5a8f7bf](https://gitlab.com/team-supercharge/oasg/commit/5a8f7bf57f13b666ff6cbcb652634352a4d8b96f))

## [4.0.0](https://gitlab.com/team-supercharge/oasg/oasg/compare/v3.2.0...v4.0.0) (2021-08-30)


### ⚠ BREAKING CHANGES

* migrate project to OSS published on npmjs.org

### Features

* migrate project to OSS published on npmjs.org ([b1f4499](https://gitlab.com/team-supercharge/oasg/oasg/commit/b1f4499060e72cfa4c2a55c642bd3b082f06858b))

## [3.2.0](https://gitlab.supercharge.io/misc/oasg/compare/v3.1.0...v3.2.0) (2021-07-20)


### Features

* introduce generatorId parameter for custom type of generator ([9a37294](https://gitlab.supercharge.io/misc/oasg/commit/9a37294b512e0783df9af5e3bd58bf710c3095a0))

## [3.1.0](https://gitlab.supercharge.io/misc/oasg/compare/v3.0.0...v3.1.0) (2021-06-21)


### Features

* add feign client generator ([4029c7f](https://gitlab.supercharge.io/misc/oasg/commit/4029c7f91cb681e0f718d929932bdcb60bb12d64))
* change feign generator to spring cloud ([889537b](https://gitlab.supercharge.io/misc/oasg/commit/889537bac2b3b77b5c69e71021eb733eb3931fbf))

## [3.0.0](https://gitlab.supercharge.io/misc/oasg/compare/v2.11.0...v3.0.0) (2021-03-23)


### ⚠ BREAKING CHANGES

* update spectral to 5.9.0

### Features

* add a number of rules for enforcing naming conventions ([0e2a425](https://gitlab.supercharge.io/misc/oasg/commit/0e2a42517250c6276bdf624ab979dace5d5c2c22))
* update spectral to 5.9.0 ([b5b639e](https://gitlab.supercharge.io/misc/oasg/commit/b5b639e9173a1dd4b618e6e7328ee46a356408bb))

## [2.11.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.10.2...v2.11.0) (2021-03-17)


### Features

* add artifactVersion CLI parameter to override generated artifact version ([9ae0c3d](https://gitlab.supercharge.io/misc/oasg/commit/9ae0c3d8b20b0c4b187c8a76e29b11ddb7290266))
* add iOS target description to README.md ([2819c9a](https://gitlab.supercharge.io/misc/oasg/commit/2819c9a76440c485dbdb0d346cddcd9d9e0a63a0))

### [2.10.2](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.10.1...v2.10.2) (2020-12-07)


### Bug Fixes

* replace referenced files with absolute paths when merging specifications ([4bb4c25](https://gitlab.supercharge.io/misc/oasg/commit/4bb4c2512523a5c45ab4dd50d00182811928c4d4))

### [2.10.1](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.10.0...v2.10.1) (2020-11-20)


### Bug Fixes

* generate more random folders for external deps ([7727426](https://gitlab.supercharge.io/misc/oasg/commit/7727426b691f02694009fc778b41f411330ec157))

## [2.10.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.9.0...v2.10.0) (2020-11-20)


### Features

* handle external references when merging specs ([1bc5249](https://gitlab.supercharge.io/misc/oasg/commit/1bc524999e2473f8a533a5e25cfa966a2834a8a9))

## [2.9.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.8.2...v2.9.0) (2020-11-19)


### Features

* introduce one-letter command aliases ([ffa2a9c](https://gitlab.supercharge.io/misc/oasg/commit/ffa2a9c72c93e94fe8fda573faaf966974c0dcd4))

### [2.8.2](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.8.1...v2.8.2) (2020-11-18)


### Bug Fixes

* add ios version parameter ([00cceb8](https://gitlab.supercharge.io/misc/oasg/commit/00cceb8fd17e7401995ddff5f24ba4b5d945aa16))

### [2.8.1](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.8.0...v2.8.1) (2020-11-17)


### Bug Fixes

* add android artifact version parameter ([2d8b956](https://gitlab.supercharge.io/misc/oasg/commit/2d8b9562e271e5faeb79270a5f90f8bfa7e05453))

## [2.8.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.7.0...v2.8.0) (2020-11-13)


### Features

* add spring-kotlin target ([6e0b598](https://gitlab.supercharge.io/misc/oasg/commit/6e0b59847aa5ce39fe34bd1a92be4a94467df06c))

## [2.7.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.6.0...v2.7.0) (2020-11-09)


### Features

* set default generator and formatter arguments for android ([180676a](https://gitlab.supercharge.io/misc/oasg/commit/180676a421a8ccdb593af91bef8ed96f4f84fe19))

## [2.6.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.5.0...v2.6.0) (2020-11-09)


### Features

* add ios target ([a6bf3ec](https://gitlab.supercharge.io/misc/oasg/commit/a6bf3ecfcb8b8ed5b8edd176e57b72418c5d7006))

## [2.5.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.4.0...v2.5.0) (2020-10-27)


### Features

* add android target ([112d51a](https://gitlab.supercharge.io/misc/oasg/commit/112d51aa0d52e7cd81e5da329bedadfa31526624))

## [2.4.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.3.1...v2.4.0) (2020-10-26)


### Features

* add proxy command to fire up a validation proxy ([876fed7](https://gitlab.supercharge.io/misc/oasg/commit/876fed7717d1e193eaf7a9b31ea20015a273894f))
* do not proxy requests from swagger-ui going to localhost ([5a73da5](https://gitlab.supercharge.io/misc/oasg/commit/5a73da5d733e2bede43552172693613814102c39))


### Bug Fixes

* rename proxy method name for future naming collision ([161b3a0](https://gitlab.supercharge.io/misc/oasg/commit/161b3a06c33191b439fcda0af60cd9c431a2e6d8))

### [2.3.1](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.3.0...v2.3.1) (2020-10-15)


### Bug Fixes

* return fail error code on warn level linter errors ([9f6c196](https://gitlab.supercharge.io/misc/oasg/commit/9f6c19681d263b3c41213e2fc7ceb248a24e5c35))

## [2.3.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.2.1...v2.3.0) (2020-10-15)


### Features

* add stubby target ([47aec45](https://gitlab.supercharge.io/misc/oasg/commit/47aec45d475ecbaa4e300656f2f55047c40cdf7b))
* check for required commands being installed on the system ([e408384](https://gitlab.supercharge.io/misc/oasg/commit/e4083842e8016d9b6bfac493d43ae08c522d2c79))
* sources can be served on localhost with swagger-ui ([a6d8e7f](https://gitlab.supercharge.io/misc/oasg/commit/a6d8e7f8eae9efd3409b69c9fae20fd787a63918))


### Bug Fixes

* handle shell command execution and failure in an uniform way ([7b0d12d](https://gitlab.supercharge.io/misc/oasg/commit/7b0d12de3ff656c031de708e4330a8651372bd6d))

### [2.2.1](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.2.0...v2.2.1) (2020-10-15)


### Bug Fixes

* **spring:** properly override package names ([8ca85c4](https://gitlab.supercharge.io/misc/oasg/commit/8ca85c40cf898d0c6a2e9dba14e3493678ad1db7))

## [2.2.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.1.0...v2.2.0) (2020-10-15)


### Features

* linter functionality using @stoplight/spectral ([8e6d5d9](https://gitlab.supercharge.io/misc/oasg/commit/8e6d5d9a93dc5301868385bd2341cec06b285516))
* only allow kebab-case or snake_case IDs for targets or sources ([ce1a1e3](https://gitlab.supercharge.io/misc/oasg/commit/ce1a1e35a83c99e02f0195dfd28257d1c9eeb592))
* set generator version to use dynamically ([f3b25c1](https://gitlab.supercharge.io/misc/oasg/commit/f3b25c1a34f8f874b09fb6be2c11775a5f912e52))


### Bug Fixes

* sources were not defined during publishing ([de14f74](https://gitlab.supercharge.io/misc/oasg/commit/de14f744dadada4136b14b963ae8511e50b8f421))
* specify proper source for publish script ([8d30108](https://gitlab.supercharge.io/misc/oasg/commit/8d30108cf54339b075c266cec354cbc557841f22))
* typo in target CLI option description ([122cadc](https://gitlab.supercharge.io/misc/oasg/commit/122cadc389d67068804f6f065e58062217d5567e))

## [2.1.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v2.0.0...v2.1.0) (2020-10-08)


### Features

* add merger and overrider ([d8a1a72](https://gitlab.supercharge.io/misc/oasg/commit/d8a1a72c9f62bd4c4f82c604a4bd32216ab9450d))
* add spring server generator ([7b9e274](https://gitlab.supercharge.io/misc/oasg/commit/7b9e2743810c9f1abfd443f5784208b3535a9a5a))
* handle configurable sources (missing merged implementation) ([e7904f4](https://gitlab.supercharge.io/misc/oasg/commit/e7904f43ced658b7bdd5db36dacdd0a2d278a21d))
* prepare override functionality ([eddefbc](https://gitlab.supercharge.io/misc/oasg/commit/eddefbc488bd12f8cdb210eb3d44aa068d652e60))

## [2.0.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.1.0...v2.0.0) (2020-10-05)


### ⚠ BREAKING CHANGES

* ability to define multiple targets of the same type

### Features

* ability to define multiple targets of the same type ([41e0804](https://gitlab.supercharge.io/misc/oasg/commit/41e08040c4cff60109d9f6047b19014f2e89c350))

## [1.1.0](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.0.6...v1.1.0) (2020-10-02)


### Features

* rename package to OASg ([24bfb9a](https://gitlab.supercharge.io/misc/oasg/commit/24bfb9aa4c3fe752a518a2c12b0c2589155f3d4f))

### [1.0.6](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.0.5...v1.0.6) (2020-09-30)


### Bug Fixes

* rename swagger to openapi, change default file location ([f42c194](https://gitlab.supercharge.io/misc/swagger-generator/commit/f42c1944aa708d3504dd55ae7c763fe169c43799))

### [1.0.5](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.0.4...v1.0.5) (2020-09-29)


### Bug Fixes

* use camelCase for folder naming as well ([057db40](https://gitlab.supercharge.io/misc/oasg/commit/057db40c96d63392dd6b7ecaf41a9ff99c00a286))

### [1.0.4](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.0.3...v1.0.4) (2020-09-29)


### Bug Fixes

* use camelCase naming everywhere ([a2276e2](https://gitlab.supercharge.io/misc/oasg/commit/a2276e2663f0bcc5b6ef6c71e1a40e7cad83ff15))

### [1.0.3](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.0.2...v1.0.3) (2020-09-29)


### Bug Fixes

* building and publishing react native bundles ([d405bc5](https://gitlab.supercharge.io/misc/oasg/commit/d405bc5985470c720dc64993bb9fd0bde1eccda0))

### [1.0.2](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.0.1...v1.0.2) (2020-09-29)


### Bug Fixes

* rename API definition file name to swagger.yaml ([e5a056a](https://gitlab.supercharge.io/misc/oasg/commit/e5a056a2c02519c7f778bfab58c344a80885ca1a))

### [1.0.1](https://gitlab.supercharge.io/misc/oasg/-/compare/v1.0.0-master-fbb709a6...v1.0.1) (2020-09-29)
