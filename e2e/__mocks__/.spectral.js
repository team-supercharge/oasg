import oasgRuleset from '@team-supercharge/oasg/ruleset';

import { schemaEnumeration } from '@team-supercharge/oasg/functions'

export default {
  extends: oasgRuleset,
  rules: {
    // in example.yaml paths are camelCased instead of the default kebab-case
    'oasg-path-casing-kebab': false,
    'oasg-path-casing-camel': true,

    // test custom functions in JS rulesets
    'valid-permissions': {
      recommended: true,
      type: 'style',
      severity: 'error',
      description: 'Property x-permissions should use values from the Permission enum',
      message: '{{error}}',
      given: '$.paths.*.*.x-permissions[*]',
      then: {
        function: schemaEnumeration,
        functionOptions: {
          schema: 'Permission',
        }
      }
    },
  }
}
