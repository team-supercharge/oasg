const execa = require("execa");
const fs = require("fs-extra");
const path = require("path");

const oasgDir = path.join(__dirname, "..");
const projectDir = path.join(oasgDir, "tmp/test-project");

module.exports = {
  createTestProject: async () => {
    await fs.remove(projectDir);
    await fs.ensureDir(projectDir);
    await execa("npm", ["init", "-y"], {
      cwd: projectDir,
    });
    await execa("npm", ["install", `@team-supercharge/oasg@${oasgDir}`], {
      cwd: projectDir,
    });
  },
  projectDir,
};
