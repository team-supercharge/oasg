const fs = require("fs-extra");
const execa = require("execa");
const { createTestProject, projectDir } = require("./setup.js");
const { copy } = require("fs-extra");
const path = require("path");

describe("oasg", () => {
  beforeAll(async () => {
    await createTestProject();
    await copy(
      path.join(__dirname, "__mocks__", "config.json"),
      path.join(projectDir, "config.json")
    );
    await copy(
      path.join(__dirname, "__mocks__", "example.yaml"),
      path.join(projectDir, "example.yaml")
    );
  }, 120000);
  /**
   * Files paths relative to the project directory to check with snapshot
   * @type {string[]}
   */
  let filesToCheck;
  beforeEach(() => {
    filesToCheck = [];
  });
  afterEach(() => {
    for (const file of filesToCheck) {
      expect(
        fs.readFileSync(path.join(projectDir, file)).toString()
      ).toMatchSnapshot();
    }
  });

  describe("lint", () => {
    afterEach(async () => {
      await fs.remove(path.join(projectDir, ".spectral.yaml"));
      await fs.remove(path.join(projectDir, ".spectral.js"));
    }, 120000);

    it("should lint with YAML ruleset", async () => {
      await copy(
        path.join(__dirname, "__mocks__", ".spectral.yaml"),
        path.join(projectDir, ".spectral.yaml")
      );
      await execa("npx", ["oasg", "lint"], {
        cwd: projectDir,
      });
    }, 120000);

    it("should lint with JS ruleset", async () => {
      await copy(
        path.join(__dirname, "__mocks__", ".spectral.js"),
        path.join(projectDir, ".spectral.js")
      );
      await execa("npx", ["oasg", "lint"], {
        cwd: projectDir,
      });
    }, 120000);
  });

  it("should generate android-simple", async () => {
    await execa("npx", ["oasg", "generate", "android-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate angular-simple", async () => {
    await execa("npx", ["oasg", "generate", "angular-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate angular-merged", async () => {
    await execa("npx", ["oasg", "generate", "angular-merged"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate swift-simple", async () => {
    await execa("npx", ["oasg", "generate", "swift-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate kmp-simple", async () => {
    await execa("npx", ["oasg", "generate", "kmp-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate flutter-simple", async () => {
    await execa("npx", ["oasg", "generate", "flutter-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate python-simple", async () => {
    await execa("npx", ["oasg", "generate", "python-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate python-legacy-simple", async () => {
    await execa("npx", ["oasg", "generate", "python-legacy-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate contract-testing-simple", async () => {
    await execa("npx", ["oasg", "generate", "contract-testing-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate server-nestjs-simple", async () => {
    await execa("npx", ["oasg", "generate", "server-nestjs-simple"], {
      cwd: projectDir,
    });
    filesToCheck.push(
      // apis
      path.join("out", "server-nestjs-simple", "api", "pet.api.ts"),
      path.join("out", "server-nestjs-simple", "api", "store.api.ts"),
      path.join("out", "server-nestjs-simple", "api", "user.api.ts"),
      // models
      path.join("out", "server-nestjs-simple", "model", "user.ts"),
      path.join("out", "server-nestjs-simple", "model", "pet.ts"),
      path.join("out", "server-nestjs-simple", "model", "order.ts"),
      path.join("out", "server-nestjs-simple", "model", "find-pets-by-status-status-parameter.ts"),

      // guards
      path.join("out", "server-nestjs-simple", "auth.guard.ts"),
    );
  }, 120000);

  it("should generate api-docs-simple", async () => {
    await execa("npx", ["oasg", "generate", "api-docs-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate feign-simple", async () => {
    await execa("npx", ["oasg", "generate", "feign-simple"], {
      cwd: projectDir,
    });
  }, 180000);

  it("should generate spring-kotlin-simple", async () => {
    await execa("npx", ["oasg", "generate", "spring-kotlin-simple"], {
      cwd: projectDir,
    });
  }, 240000);

  it.skip("should generate spring-simple", async () => {
    await execa("npx", ["oasg", "generate", "spring-simple"], {
      cwd: projectDir,
    });
  }, 240000);

  it.skip("should generate stubby-simple", async () => {
    await execa("npx", ["oasg", "generate", "stubby-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it("should generate react-simple", async () => {
    await execa("npx", ["oasg", "generate", "react-simple"], {
      cwd: projectDir,
    });
    filesToCheck.push(
      path.join("out", "react-simple", "src", "apis", "use-api.hook.tsx"),
      path.join("out", "react-simple", "src", "apis", "PetApi.hook.ts"),
      path.join("out", "react-simple", "src", "apis", "StoreApi.hook.ts"),
      path.join("out", "react-simple", "src", "apis", "UserApi.hook.ts")
    );
  }, 120000);

  it("should generate feign-kotlin-simple", async () => {
    await execa("npx", ["oasg", "generate", "feign-kotlin-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it.skip("should generate postman-simple", async () => {
    await execa("npx", ["oasg", "generate", "postman-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it.skip("should generate client-typscript-axios-simple", async () => {
    await execa("npx", ["oasg", "generate", "client-typescript-axios-simple"], {
      cwd: projectDir,
    });
  }, 120000);

  it.skip("should generate client-typscript-fetch-simple", async () => {
    await execa("npx", ["oasg", "generate", "client-typescript-fetch-simple"], {
      cwd: projectDir,
    });
  }, 120000);
});
