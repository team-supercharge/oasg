FROM node:20.17.0

WORKDIR /app
COPY . .
RUN npm ci

# set ENV variables
ENV DART_SDK=/usr/lib/dart
ENV PATH=$DART_SDK/bin:$PATH

RUN chmod +x setup.sh && ./setup.sh

WORKDIR /
RUN npm install -g /app

# install cli dependencies
RUN npm install -g @stoplight/spectral-cli@6.14.1
RUN npm install -g @stoplight/prism-cli@5.12.0
RUN npm install -g @redocly/cli@1.25.11
