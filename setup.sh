# basic packages
apt update && apt upgrade -y && apt install -y software-properties-common apt-transport-https ca-certificates

# OpenJDK for running openapi-generator
mkdir -p /etc/apt/keyrings
wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public | tee /etc/apt/keyrings/adoptium.asc
echo "deb [signed-by=/etc/apt/keyrings/adoptium.asc] https://packages.adoptium.net/artifactory/deb $(awk -F= '/^VERSION_CODENAME/{print$2}' /etc/os-release) main" | tee /etc/apt/sources.list.d/adoptium.list
apt update && apt install -y temurin-17-jdk
apt update && apt install -y temurin-11-jdk
update-java-alternatives -s temurin-17-jdk-amd64

# jq for parsing JSON configuration in bash
apt update && apt install -y jq

# pip3 and twine to upload PyPI artifacts
apt update && apt install -y python3-pip
apt update && apt install -y python3-venv
pip3 install twine --break-system-packages

# install dart
case "$(dpkg --print-architecture)" in
    amd64) SDK_ARCH="x64";;
    armhf) SDK_ARCH="arm";;
    arm64) SDK_ARCH="arm64";;
esac;
export SDK="dartsdk-linux-${SDK_ARCH}-release.zip"
export BASEURL="https://storage.googleapis.com/dart-archive/channels"
export URL="$BASEURL/stable/release/3.0.5/sdk/$SDK"
echo "SDK: $URL" >> dart_setup.log
curl -fLO "$URL"
unzip "$SDK" && mv dart-sdk "$DART_SDK" && rm "$SDK" && chmod 755 "$DART_SDK" && chmod 755 "$DART_SDK/bin";

wget https://dot.net/v1/dotnet-install.sh -O dotnet-install.sh
chmod +x ./dotnet-install.sh
./dotnet-install.sh --runtime dotnet --channel 6.0
